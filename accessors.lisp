(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

(defgeneric $ (collection location &rest others-and-default))
(defgeneric (setf $) (value collection location &rest others))
(defgeneric $count (s))

(defmethod $ ((list list) index &rest default)
  (typecase index
    (number (let ((v (nth index list)))
              (or v (car default))))
    (keyword (let ((v (getf list index)))
               (or v (car default))))))

(defmethod $ ((array array) index &rest default)
  (if (and (>= index 0) (< index (length array)))
      (aref array index)
      (car default)))

(defmethod $ ((ht hash-table) key &rest default)
  (gethash key ht (car default)))

(defmethod $ ((string string) index &rest default)
  (if (and (>= index 0) (< index (length string)))
      (char string index)
      (car default)))

(defmethod $ ((object standard-object) location &rest default)
  (let ((v (slot-value object location)))
    (if v
        v
        (car default))))

(defmethod $ ((symbol symbol) location &rest default)
  (get location symbol (car default)))

(defmethod (setf $) (value (list list) index &rest others)
  (declare (ignore others))
  (typecase index
    (number (setf (nth index list) value))
    (keyword (setf (getf list index) value))))

(defmethod (setf $) (value (array array) index &rest others)
  (declare (ignore others))
  (setf (aref array index) value))

(defmethod (setf $) (value (ht hash-table) key &rest others)
  (declare (ignore others))
  (setf (gethash key ht) value))

(defmethod (setf $) (value (string string) index &rest others)
  (declare (ignore others))
  (setf (char string index) value))

(defmethod (setf $) (value (object standard-object) location &rest others)
  (declare (ignore others))
  (setf (slot-value object location) value))

(defmethod (setf $) (value (symbol symbol) location &rest others)
  (declare (ignore others))
  (setf (get location symbol) value))

(defun $first (collection) ($ collection 0))
(defun $last (collection) ($ collection (1- ($count collection))))
(defun $second (collection) ($ collection 1))
(defun $third (collection) ($ collection 2))
(defun $fourth (collection) ($ collection 3))
(defun $fifth (collection) ($ collection 4))
(defun $sixth (collection) ($ collection 5))
(defun $seventh (collection) ($ collection 6))
(defun $eighth (collection) ($ collection 7))
(defun $nineth (collection) ($ collection 8))
(defun $tenth (collection) ($ collection 9))

(defun $0 (collection) ($ collection 0))
(defun $1 (collection) ($ collection 1))
(defun $2 (collection) ($ collection 2))
(defun $3 (collection) ($ collection 3))
(defun $4 (collection) ($ collection 4))
(defun $5 (collection) ($ collection 5))
(defun $6 (collection) ($ collection 6))
(defun $7 (collection) ($ collection 7))
(defun $8 (collection) ($ collection 8))
(defun $9 (collection) ($ collection 9))
