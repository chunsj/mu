(in-package :mu)

(defclass dataframe ()
  ((names :initarg :names
          :documentation "column names"
          :type (simple-array string (*))
          :reader df/names)
   (types :initarg :types
          :documentation "a list of column types"
          :type simple-vector
          :reader df/types)
   (columns :initarg :columns
            :documentation "a vector of column vectors"
            :type (simple-array simple-vector (*))
            :reader df/columns)
   (index :initarg :index
          :initform nil
          :documentation "the name for the row index")
   (positions :type hash-table
              :initform (make-hash-table :test #'equal :synchronized T)
              :reader df/positions)))

(defun df/ncols (df) ($count (df/columns df)))

(defun df/nrows (df)
  (let ((columns (df/columns df)))
    (if (> ($count columns) 0)
        ($count ($ columns 0))
        0)))

(defun df/make (names &key rows columns types index)
  (when (and rows columns) (error "use only :rows or :columns argument"))
  (when rows
    (unless (= ($count names) ($count (first rows)))
      (error "names and columns should have the same length")))
  (when columns
    (unless (= ($count names) ($count columns))
      (error "names and columns should have the same length")))
  (when index
    (unless (member index names :test #'string-equal)
      (error "index should be one of the names")))
  (let* ((types (coerce (cond (types (unless (= ($count names) ($count types))
                                       (error "names and types should have the same length"))
                                     types)
                              (rows (mapcar (lambda (v)
                                              (cond ((numberp v) 'number)
                                                    ((stringp v) 'string)
                                                    (T T)))
                                            (first rows)))
                              (T (loop :for column :in columns
                                       :for first-element = (first column)
                                       :collect (cond ((numberp first-element) 'number)
                                                      ((stringp first-element) 'string)
                                                      (T T)))))
                        'vector))
         (columns (coerce (cond (rows (loop :with num-rows = ($count rows)
                                            :for item :in (first rows)
                                            :for element-type :across types
                                            :collect (make-array num-rows
                                                                 :element-type element-type
                                                                 :adjustable nil
                                                                 :fill-pointer nil)))
                                (columns (mapcar (lambda (vs) (coerce vs 'vector)) columns)))
                          'vector))
         (names (coerce names 'vector))
         (df (make-instance 'dataframe :names names :types types :columns columns)))
    (when rows
      (loop :for row :in rows
            :for row-idx :of-type fixnum :upfrom 0
            :do (loop :for element :in row
                      :for idx :upfrom 0
                      :for column :across columns
                      :for column-name :across names
                      :do (setf ($ column row-idx) element))))
    (when index
      (let ((ix index))
        (with-slots (index) df (setf index ix)))
      (let ((index-column (df/column df index)))
        (loop :for val :across index-column
              :for row-idx :from 0
              :do (let ((pidx ($ (df/positions df) val)))
                    (when pidx
                      (error "duplicated index: rows ~A and ~A has the ~S value"
                             pidx row-idx val))
                    (setf ($ (df/positions df) val) row-idx)))))
    df))

(defun df/from.plist (plst &key index)
  (when-let ((first.row (car plst)))
    (let ((names (loop :for key :in first.row :by #'cddr :collect key)))
      (let ((columns (mapcar (lambda (name) (mapcar (lambda (e) ($ e name)) plst))
                             names)))
        (df/make names :columns columns :index index)))))

(defun df/save (df &key (filename "DF.csv"))
  (when df
    (let ((names (df/names df))
          (columns (df/columns df)))
      (with-open-file (fs filename :direction :output :if-exists :supersede)
        (format fs "~{~A~^,~}~%" (coerce names 'list))
        (let ((nc ($count columns))
              (nr ($count ($0 columns))))
          (loop :for i :from 0 :below nr
                :do (let ((row (loop :for j :from 0 :below nc
                                     :collect ($ ($ columns j) i))))
                      (format fs "~{~A~^,~}~%" row))))))))

(defun df/index (df) (with-slots (index) df index))

(defun (setf df/index) (df index)
  (when index
    (let ((index-column (df/column df index))
          (ix index))
      (with-slots (positions index) df
        (setf positions (make-hash-table :test #'equal :synchronized T))
        (setf index ix))
      (loop :for val :across index-column
            :for row-idx :from 0
            :do (let ((pidx ($ (df/positions df) val)))
                  (when pidx
                    (error "duplicated index: rows ~A and ~A has the ~S value"
                           pidx row-idx val))
                  (setf ($ (df/positions df) val) row-idx)))
      index)))

(defun df/iterator (df)
  (let ((idx 0))
    (lambda ()
      (when (< idx (df/nrows df))
        (prog1 (loop :for column :across (df/columns df) :collect ($ column idx))
          (incf idx))))))

(defun df/next (iterator) (funcall iterator))

(defun df/slice (df &key columns from to)
  (let ((nrows (1- (df/nrows df)))
        (columns (coerce (or columns (df/names df)) 'list)))
    (flet ((convert-negative-idx (idx)
             (if (< idx 0) (+ nrows idx) idx)))
      (loop :with columns = columns
            :with slice = (when (or from to)
                            (cons (convert-negative-idx (or from 0))
                                  (convert-negative-idx (or to nrows))))
            :for name :across (df/names df)
            :for type :across (df/types df)
            :for col :across (df/columns df)
            :when (or (null columns)
                      (member name columns :test #'string-equal))
              :collect name :into new-names
              :and :collect type :into new-types
              :and :collect (if slice
                                (subseq col (car slice) (1+ (cdr slice)))
                                col)
                     :into new-columns
            :finally (return (df/make new-names :columns new-columns
                                                :types new-types
                                                :index (when (df/index df)
                                                         (when (member (df/index df)
                                                                       new-names
                                                                       :test #'string-equal)))))))))

(defun df/head (df &optional (rows 10)) (df/slice df :to rows))
(defun df/tail (df &optional (rows 10)) (df/slice df :from (- rows)))

(defun df/column (df name &key (as :vector))
  (loop :for column-name :across (df/names df)
        :for column :across (df/columns df)
        :when (string-equal name column-name)
          :do (return
                (ecase as
                  (:vector column)
                  (:list (coerce column 'list))))
        :finally (error "column ~S not found" name)))

(defun df/column.idx (df name)
  (loop :for column-name :across (df/names df)
        :for idx :from 0
        :when (string-equal name column-name)
          :do (return idx)
        :finally (error "column ~S not found" name)))

(defun df/row.idx (df name) ($ (df/positions df) name))

(defun df/scalar (df row-idx column-name)
  (let* ((column-idx (df/column.idx df column-name))
         (columns (df/columns df))
         (column ($ columns column-idx))
         (max-idx (1- ($count column))))
    (when (> row-idx max-idx)
      (error "index should be in [0, ~A] range" max-idx))
    ($ column row-idx)))

(defun df/row (df row.location &key (as :vector))
  (let ((row.idx (df/row.idx df row.location))
        (index.idx -1))
    (if row.idx
        (setf index.idx (df/column.idx df (df/index df)))
        (setf row.idx row.location))
    (when (numberp row.idx)
      (coerce (loop :for column :across (df/columns df)
                    :for col.idx :from 0
                    :unless (= col.idx index.idx)
                      :collect ($ column row.idx))
              (ecase as
                (:vector 'vector)
                (:list 'list))))))

(defmethod $ ((df dataframe) location &rest others-and-default)
  (if (= 0 ($count others-and-default))
      (df/column df location)
      (let ((row.idx (df/row.idx df location))
            (column (car others-and-default))
            (default (cadr others-and-default)))
        (if row.idx
            (let ((val (df/scalar df row.idx column)))
              (or val default))
            (let ((val (df/scalar df location column)))
              (or val default))))))

(defmethod print-object ((df dataframe) stream)
  (let ((names (->> (coerce (df/names df) 'list)
                    (tx/map (lambda (n)
                              (if (string-equal n (df/index df))
                                  (format nil "~A[*]" n)
                                  n))))))
    (loop :with tbl = (xtbl (append '("") names))
          :with iterator = (df/iterator df)
          :for i :from 0
          :for row = (funcall iterator) :then (funcall iterator)
          :while row
          :do (xtbl/row tbl (append (list i) row))
          :finally (xtbl/display tbl stream))))

(defun df/stats (df)
  (df/make '(:column :min :p25 :p50 :p75 :max :mean :sd :sum)
           :rows (loop :for name :across (df/names df)
                       :for column :across (df/columns df)
                       :for type :across (df/types df)
                       :when (subtypep type 'number)
                         :collect (list name
                                        (reduce #'min column)
                                        (percentile column 25)
                                        (percentile column 50)
                                        (percentile column 75)
                                        (reduce #'max column)
                                        (coerce (mean (coerce column 'list)) 'single-float)
                                        (coerce (sd (coerce column 'list)) 'single-float)
                                        (reduce #'+ column)))))

(defun df/drop (df n)
  (df/make (coerce (df/names df) 'list)
           :index (df/index df)
           :rows (loop :with iterator = (df/iterator df)
                       :for row = (funcall iterator) :then (funcall iterator)
                       :for i :from 0
                       :while row
                       :when (and (not (member nil row)) (>= i n))
                         :collect row)))

(defun df/drop.nil (df)
  (df/make (coerce (df/names df) 'list)
           :index (df/index df)
           :rows (loop :with iterator = (df/iterator df)
                       :for row = (funcall iterator) :then (funcall iterator)
                       :while row
                       :when (not (member nil row))
                         :collect row)))

(defun df/take (df n)
  (df/make (coerce (df/names df) 'list)
           :index (df/index df)
           :rows (loop :with iterator = (df/iterator df)
                       :for row = (funcall iterator) :then (funcall iterator)
                       :for i :from 0
                       :while row
                       :when (and (not (member nil row)) (< i n))
                         :collect row)))


(defun df/mappend (df name fn)
  (when-let ((rows (loop :with iterator = (df/iterator df)
                         :with col.names = (df/names df)
                         :with col.indices = (let ((map #{}))
                                               (loop :for cname :across col.names
                                                     :for cidx = (df/column.idx df cname)
                                                     :do (setf ($ map cname) cidx))
                                               map)
                         :for row = (funcall iterator) :then (funcall iterator)
                         :while row
                         :for rm = (let ((rm #{}))
                                     (loop :for cn :across col.names
                                           :for i = ($ col.indices cn)
                                           :do (setf ($ rm cn) ($ row i)))
                                     rm)
                         :collect (append row (list (funcall fn rm))))))
    (df/make (append (coerce (df/names df) 'list)
                     (list name))
             :index (df/index df)
             :rows rows)))

(defun df/mappend.rolling (df name nroll fn)
  (when-let ((rows (let ((rows nil))
                     (loop :with iterator = (df/iterator df)
                           :with col.names = (df/names df)
                           :with index = (df/index df)
                           :with col.indices = (let ((map #{}))
                                                 (loop :for cname :across col.names
                                                       :for cidx = (df/column.idx df cname)
                                                       :do (setf ($ map cname) cidx))
                                                 map)
                           :for k :from 1
                           :for row = (df/next iterator)
                           :while row
                           :do (progn
                                 (push row rows)
                                 (when (> ($count rows) nroll)
                                   (setf rows (tx/take nroll rows))))
                           :collect (if (>= ($count rows) nroll)
                                        (append row (list (funcall fn
                                                                   (df/make (coerce col.names 'list)
                                                                            :index index
                                                                            :rows (reverse rows)))))
                                        (append row (list nil)))))))
    (df/make (append (coerce (df/names df) 'list)
                     (list name))
             :index (df/index df)
             :rows rows)))

(defun df/map (df names fn)
  (when-let ((rows (let ((col.idx (df/column.idx df (df/index df))))
                     (loop :with iterator = (df/iterator df)
                           :with col.names = (df/names df)
                           :with idx = (df/index df)
                           :with col.indices = (let ((map #{}))
                                                 (loop :for cname :across col.names
                                                       :for cidx = (df/column.idx df cname)
                                                       :do (setf ($ map cname) cidx))
                                                 map)
                           :for row = (funcall iterator) :then (funcall iterator)
                           :while row
                           :for rm = (let ((rm #{}))
                                       (loop :for cn :across col.names
                                             :for i = ($ col.indices cn)
                                             :do (setf ($ rm cn) ($ row i)))
                                       rm)
                           :collect (let ((idx ($ row col.idx)))
                                      (append (list idx)
                                              (funcall fn rm)))))))
    (df/make (append (list (df/index df)) names)
             :index (df/index df)
             :rows rows)))

(defun df/map.rolling (df names nroll fn)
  (when-let ((rows (let ((rows nil)
                         (col.idx (df/column.idx df (df/index df)))
                         (nils (loop :for n :in names :collect nil)))
                     (loop :with iterator = (df/iterator df)
                           :with col.names = (df/names df)
                           :with index = (df/index df)
                           :with col.indices = (let ((map #{}))
                                                 (loop :for cname :across col.names
                                                       :for cidx = (df/column.idx df cname)
                                                       :do (setf ($ map cname) cidx))
                                                 map)
                           :for k :from 1
                           :for row = (df/next iterator)
                           :while row
                           :do (progn
                                 (push row rows)
                                 (when (> ($count rows) nroll)
                                   (setf rows (tx/take nroll rows))))
                           :collect (let ((idx ($ row col.idx)))
                                      (if (>= ($count rows) nroll)
                                          (append (list idx) (funcall fn
                                                                      (df/make (coerce col.names 'list)
                                                                               :index index
                                                                               :rows (reverse rows))))
                                          (append (list idx) nils)))))))
    (df/make (append (list (df/index df)) names)
             :index (df/index df)
             :rows rows)))

(defun df/mdd (df column)
  (let ((hwm nil)
        (mdd 0)
        (hwmi nil)
        (mdd0 0)
        (mddi 0))
    (df/map df `(,column "HWM" "HWM.DATE" "MDD" "MDD.START" "MDD.DATE")
            (lambda (row)
              (let ((date ($ row "DATE"))
                    (v ($ row column)))
                (unless hwmi (setf hwmi date))
                (unless hwm (setf hwm v))
                (let ((d (/ (- hwm v) hwm)))
                  (when (>= d mdd)
                    (setf mddi date)
                    (setf mdd0 hwmi)
                    (setf mdd d))
                  (when (>= v hwm)
                    (setf hwmi date)
                    (setf hwm v))
                  (list v hwm hwmi (ipct mdd) mdd0 mddi)))))))

(defun df/correlation (df colkey1 colkey2 &optional (truncate T))
  (when-let ((vs1 (df/column df colkey1))
             (vs2 (df/column df colkey2)))
    (let ((m1 (mean vs1))
          (m2 (mean vs2)))
      (let ((v (/ (sum (map 'list (lambda (v1 v2) (* (- v1 m1) (- v2 m2))) vs1 vs2))
                  (sqrt (* (sum (map 'list (lambda (v1) (* (- v1 m1) (- v1 m1))) vs1))
                           (sum (map 'list (lambda (v2) (* (- v2 m2) (- v2 m2))) vs2)))))))
        (if truncate
            (if (< (abs v) 9E-4)
                (* (/ v (abs v)) 0)
                (/ (round (* 1E8 v) 1E4) 1E4))
            v)))))

(defun df/correlations (df &key columns (truncate T) verbose)
  (when-let ((df (df/drop.nil df)))
    (let ((columns (or columns
                       (loop :for type :across (df/types df)
                             :for name :across (df/names df)
                             :when (eq type 'number)
                               :collect name)))
          (done #{}))
      (let ((data (loop :for col1 :in columns
                        :collect (append (list :code col1)
                                         (loop :for col2 :in columns
                                               :for k = (if (string< col1 col2)
                                                            ($concat col1 col2)
                                                            ($concat col2 col1))
                                               :for x = ($ done k)
                                               :for corr = (if (string-equal col1 col2)
                                                               1
                                                               (if x
                                                                   ""
                                                                   (df/correlation df
                                                                                   col1 col2
                                                                                   truncate)))
                                               :appending (progn
                                                            (setf ($ done k) T)
                                                            (list col2 corr)))))))
        (when verbose (prn/table data))
        (make-array (list ($count columns) ($count columns))
                    :initial-contents (loop :for rec :in data
                                            :for row = (cddr rec)
                                            :collect (loop :for e :in row
                                                           :for i :from 0
                                                           :when (oddp i)
                                                             :collect (if (equal "" e)
                                                                          0
                                                                          e))))))))

(defmacro with/df.rows ((df &key (drop.nil T) (collect T)) &body body)
  ;; W/DF, W/ROW
  `(let ((__pd.df__ (if ,drop.nil
                        (df/drop.nil ,df)
                        ,df)))
     (let ((w/df __pd.df__))
       (if ,collect
           (loop :with __iterator__ = (df/iterator __pd.df__)
                 :with __col.names__ = (df/names __pd.df__)
                 :with __col.indices__ = (let ((map #{}))
                                           (loop :for cname :across __col.names__
                                                 :for cidx = (df/column.idx __pd.df__ cname)
                                                 :do (setf ($ map cname) cidx))
                                           map)
                 :for __row__ = (df/next __iterator__)
                 :while __row__
                 :for w/row = (let ((rm #{}))
                                (loop :for cn :across __col.names__
                                      :for i = ($ __col.indices__ cn)
                                      :do (setf ($ rm cn) ($ __row__ i)))
                                rm)
                 :collect (labels (($df () w/df)
                                   ($row (&optional k) (if (null k) w/row ($ w/row k))))
                            (declare (ignorable $df $row))
                            ,@body))
           (loop :with __iterator__ = (df/iterator __pd.df__)
                 :with __col.names__ = (df/names __pd.df__)
                 :with __col.indices__ = (let ((map #{}))
                                           (loop :for cname :across __col.names__
                                                 :for cidx = (df/column.idx __pd.df__ cname)
                                                 :do (setf ($ map cname) cidx))
                                           map)
                 :for __row__ = (df/next __iterator__)
                 :while __row__
                 :for w/row = (let ((rm #{}))
                                (loop :for cn :across __col.names__
                                      :for i = ($ __col.indices__ cn)
                                      :do (setf ($ rm cn) ($ __row__ i)))
                                rm)
                 :do (labels (($df () w/df)
                              ($row (&optional k) (if (null k) w/row ($ w/row k))))
                       (declare (ignorable $df $row))
                       ,@body))))))

(defun df/filter (df function)
  (when-let ((rows (loop :with iter = (df/iterator df)
                         :with col.names = (df/names df)
                         :with col.indices = (let ((map #{}))
                                               (loop :for cname :across col.names
                                                     :for cidx = (df/column.idx df cname)
                                                     :do (setf ($ map cname) cidx))
                                               map)
                         :for row = (df/next iter)
                         :while row
                         :for res = (when row
                                      (let ((rm #{}))
                                        (loop :for cn :across col.names
                                              :for i = ($ col.indices cn)
                                              :do (setf ($ rm cn) ($ row i)))
                                        (when (funcall function rm)
                                          row)))
                         :when res
                           :collect res)))
    (df/make (coerce (df/names df) 'list)
             :index (df/index df)
             :rows rows)))
