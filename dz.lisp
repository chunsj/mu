(in-package :mu.dz)

(defparameter *node-enable-backprop* T)

(defmacro with-backprop ((&optional (sw T)) &body body)
  `(let ((mu.dz::*node-enable-backprop* ,sw))
     ,@body))

(defgeneric node/tag (node))
(defgeneric node/data (node))
(defgeneric node/computer (node))
(defgeneric node/gradient (node))
(defgeneric node/backprop (node &key retain graph))
(defgeneric node/gradient! (node))

(defclass node ()
  ((tag :initform :node :accessor node/tag)
   (gen :initform 0 :accessor node/generation)
   (data :initform nil :accessor node/data)))

(defclass node/variable (node)
  ((grad :initform nil :accessor node/gradient)))

(defclass node/compute (node/variable)
  ((input :initform nil :accessor node/input)
   (dfn :initform nil :accessor node/derivative)))

(defun node/variable (data &key tag gradient)
  (let ((node (make-instance 'node/variable)))
    (setf (node/tag node) (or tag :variable))
    (setf (node/data node) data)
    (when gradient (setf (node/gradient node) gradient))
    node))

(defun node/compute (input data dfn &key tag)
  (if *node-enable-backprop*
      (let ((node (make-instance 'node/compute)))
        (setf (node/tag node) (or tag :compute))
        (setf (node/data node) data)
        (setf (node/input node) (if (listp input) input (list input)))
        (setf (node/generation node) (1+ (node/generation input)))
        (setf (node/derivative node) dfn)
        node)
      (node/variable data :tag tag)))

(defmethod node/computer ((node node)) nil)
(defmethod node/computer ((node node/compute)) node)

(defmethod node/generation ((nodes list))
  (apply #'max (mapcar #'node/generation nodes)))

(defmethod node/backprop ((node node/variable) &key retain graph)
  (declare (ignore node retain graph)))

(defmethod node/backprop ((node node/compute) &key retain graph)
  (let ((computers nil)
        (seen nil))
    (labels ((generation/sorted (computers computer)
               (if (null computers)
                   (cons computer nil)
                   (if (> (node/generation computer) (node/generation (car computers)))
                       (cons computer computers)
                       (cons (car computers) (generation/sorted (cdr computers) computer)))))
             (computers/add (node)
               (when-let ((computer (node/computer node)))
                 (unless (member computer seen)
                   (push computer seen)
                   (setf computers (generation/sorted computers computer)))))
             (gradients/compute (node)
               ;; XXX SET WITH THE SAME SHAPE
               (unless (node/gradient node) (setf (node/gradient node) (node/variable 1.0)))
               (let ((gradients (funcall (node/derivative node) node (node/gradient node))))
                 (if (listp gradients)
                     gradients
                     (list gradients))))
             (gradient/assign (node gradient)
               (if (node/gradient node)
                   (setf (node/gradient node) (n/add (node/gradient node)
                                                     gradient))
                   (setf (node/gradient node) gradient)))
             (gradient/clear (node)
               (unless retain (setf (node/gradient node) nil))))
      (computers/add node)
      (loop :while computers
            :for cmp = (pop computers)
            :do (with-backprop (graph)
                  (let ((xs (node/input cmp))
                        (gradients (gradients/compute cmp)))
                    (loop :for x :in xs
                          :for gradient :in gradients
                          :do (progn
                                (gradient/assign x gradient)
                                (computers/add x)
                                (gradient/clear cmp)))))))))

(defmethod node/gradient! ((node node/variable))
  (setf (node/gradient node) nil))

(defmethod print-object ((node node) stream)
  (cond ((numberp (node/data node))
         (format stream "#<~A(~A|~A) ~A|~A>" (type-of node) (node/tag node) (node/generation node)
                 (node/data node) (node/gradient node)))
        (T (format stream "#<~A(~A)>" (type-of node) (node/tag node)))))

(defun node/input.data (node) (node/data (car (node/input node))))

(defgeneric n/square (x))
(defgeneric n/exp (x))

(defmethod n/square ((x number)) (expt x 2))

(defmethod n/square ((x node))
  (let ((data (node/data x)))
    (node/compute x
                  (expt data 2)
                  (lambda (node bw)
                    (n* 2 node bw))
                  :tag :square)))

(defmethod n/exp ((x number)) (exp x))

(defmethod n/exp ((x node))
  (let ((data (node/data x)))
    (node/compute x
                  (exp data)
                  (lambda (node bw)
                    (n/mul (n/exp node) bw))
                  :tag :exp)))

(defgeneric n/add (x y))

(defmethod n/add ((x number) (y number)) (+ x y))

(defmethod n/add ((x node) (y number))
  (let ((xdata (node/data x))
        (ydata y))
    (node/compute (list x)
                  (+ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list bw))
                  :tag :add)))

(defmethod n/add ((x number) (y node))
  (let ((xdata x)
        (ydata (node/data y)))
    (node/compute (list y)
                  (+ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list bw))
                  :tag :add)))

(defmethod n/add ((x node) (y node))
  (let ((xdata (node/data x))
        (ydata (node/data y)))
    (node/compute (list x y)
                  (+ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list bw bw))
                  :tag :add)))

(defgeneric n/mul (x y))

(defmethod n/mul ((x number) (y number)) (* x y))

(defmethod n/mul ((x node) (y number))
  (let ((xdata (node/data x))
        (ydata y))
    (node/compute (list x)
                  (* xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/mul y bw)))
                  :tag :mul)))

(defmethod n/mul ((x number) (y node))
  (let ((xdata x)
        (ydata (node/data y)))
    (node/compute (list y)
                  (* xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/mul x bw)))
                  :tag :mul)))

(defmethod n/mul ((x node) (y node))
  (let ((xdata (node/data x))
        (ydata (node/data y)))
    (node/compute (list x y)
                  (* xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/mul y bw) (n/mul x bw)))
                  :tag :mul)))

(defun n+ (&rest nodes) (reduce #'n/add nodes))
(defun n* (&rest nodes) (reduce #'n/mul nodes))

(defgeneric n/neg (x))
(defgeneric n/sub (x y))

(defmethod n/neg ((x number)) (- x))

(defmethod n/neg ((x node))
  (let ((xdata (node/data x)))
    (node/compute (list x)
                  (- xdata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/neg bw)))
                  :tag :neg)))

(defmethod n/sub ((x number) (y number)) (- x y))

(defmethod n/sub ((x node) (y number))
  (let ((xdata (node/data x))
        (ydata y))
    (node/compute (list x)
                  (- xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list bw))
                  :tag :sub)))

(defmethod n/sub ((x number) (y node))
  (let ((xdata x)
        (ydata (node/data y)))
    (node/compute (list y)
                  (- xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/neg bw)))
                  :tag :sub)))

(defmethod n/sub ((x node) (y node))
  (let ((xdata (node/data x))
        (ydata (node/data y)))
    (node/compute (list x y)
                  (- xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list bw (n/neg bw)))
                  :tag :sub)))

(defun n- (&rest nodes)
  (if (= 1 (length nodes))
      (n/neg (car nodes))
      (reduce #'n/sub nodes)))

(defgeneric n/div (x y))

(defmethod n/div ((x number) (y number)) (/ x y))

(defmethod n/div ((x node) (y number))
  (let ((xdata (node/data x))
        (ydata y))
    (node/compute (list x)
                  (/ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/div bw y)))
                  :tag :div)))

(defmethod n/div ((x number) (y node))
  (let ((xdata x)
        (ydata (node/data y)))
    (node/compute (list y)
                  (/ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/mul bw (n/div (n/neg x) (n/expt y 2)))))
                  :tag :div)))

(defmethod n/div ((x node) (y node))
  (let ((xdata (node/data x))
        (ydata (node/data y)))
    (node/compute (list x y)
                  (/ xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/div bw y) (n/mul bw (n/div (n/neg x) (n/expt y 2)))))
                  :tag :div)))

(defun n/ (&rest nodes) (reduce #'n/div nodes))

(defgeneric n/expt (x y))

(defmethod n/expt ((x number) (y number)) (expt x y))

(defmethod n/expt ((x node) (y number))
  (let ((xdata (node/data x))
        (ydata y))
    (node/compute (list x)
                  (expt xdata ydata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n* bw y (n/expt x (- ydata 1)))))
                  :tag :expt)))

(defgeneric n/sin (x))

(defmethod n/sin ((x number)) (sin x))

(defmethod n/sin ((x node))
  (let ((xdata (node/data x)))
    (node/compute (list x)
                  (sin xdata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n/mul bw (n/cos x))))
                  :tag :sin)))

(defgeneric n/cos (x))

(defmethod n/cos ((x number)) (cos x))

(defmethod n/cos ((x node))
  (let ((xdata (node/data x)))
    (node/compute (list x)
                  (cos xdata)
                  (lambda (node bw)
                    (declare (ignore node))
                    (list (n* bw -1 (n/sin x))))
                  :tag :cos)))

(defgeneric n/tanh (x))

(defmethod n/tanh ((x node))
  (let ((v (tanh (node/data x))))
    (node/compute (list x)
                  v
                  (lambda (node bw)
                    (list (n/mul bw (n/sub 1.0 (n/mul node node))))))))
