(in-package :mu)

(defun mu/setup.kernel (&optional verbose)
  (unless lparallel:*kernel*
    (let ((ncpu-cores (cl-cpus:get-number-of-processors)))
      (when verbose (prn "MU::KERNEL CORES AS" ncpu-cores))
      (setf lparallel:*kernel* (lparallel:make-kernel ncpu-cores)))))

(defun mu/end.kernel (&optional (wait T))
  (when lparallel:*kernel*
    (lparallel:end-kernel :wait wait)))

#+sbcl
(defun mu/make.exe (name main &key (exe T) prebuild verbose)
  (when prebuild (funcall prebuild))
  (mu/end.kernel)
  (let ((real-main (lambda () (funcall main (cdr sb-ext:*posix-argv*)))))
    (when verbose (prn "MAKING EXECUTABLE:" name "..."))
    (sb-ext:save-lisp-and-die name :toplevel real-main :executable exe)))
