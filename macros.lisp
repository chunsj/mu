(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

;; #[...] is almost same as #(...) only it does evaluate the included expressions
(defun vector-reader (stream char arg)
  (declare (ignore char arg))
  (let ((vals (read-delimited-list #\] stream T)))
    `(make-array ,(length vals) :initial-contents (list ,@vals) :adjustable T)))

;; #{...} is for creating hash table with equalp test
(defun hash-table-reader (stream char arg)
  (declare (ignore char arg))
  (let ((kvs (read-delimited-list #\} stream T))
        (ht (gensym)))
    `(let ((,ht (make-hash-table :test 'equal)))
       ,@(loop :for tail :on kvs :by #'cddr :while kvs
               :collect `(setf (gethash ,(car tail) ,ht) ,(cadr tail)))
       ,ht)))

(defun read-delimiter (stream char)
  (declare (ignore stream))
  (error "Delimiter ~S shouldn't be read alone" char))

(set-dispatch-macro-character #\# #\[ #'vector-reader)
(set-dispatch-macro-character #\# #\{ #'hash-table-reader)

(set-macro-character #\] 'read-delimiter)
(set-macro-character #\} 'read-delimiter)

(set-pprint-dispatch
 'hash-table
 (lambda (str ht)
   (let* ((keys (loop :for key :being :the :hash-keys :of ht :collect key))
          (nkeys (length keys))
          (plength (if *PRINT-LENGTH* *PRINT-LENGTH* 1000000))
          (skeys (if (> nkeys (/ plength 2))
                     (loop :for i :from 0 :to (1- (/ plength 2))
                           :collect (nth i keys))
                     keys))
          (strs (loop :for key :in skeys
                      :collect (format nil "~S ~S" key (gethash key ht)))))
     (if (> nkeys (length strs))
         (format str "#{~{~A~^ ~} ...}" strs)
         (format str "#{~{~A~^ ~}}" strs)))))

(defmacro -> (initial-form &rest forms)
  "clojure like thread macro with first argument"
  (let ((output-form initial-form)
        (remaining-forms forms))
    (loop while remaining-forms do
      (let ((current-form (car remaining-forms)))
        (if (listp current-form)
            (setf output-form (cons (car current-form)
                                    (cons output-form (cdr current-form))))
            (setf output-form (list current-form output-form))))
      (setf remaining-forms (cdr remaining-forms)))
    output-form))

(defmacro ->> (initial-form &rest forms)
  "clojure like thread macro with last argument"
  (let ((output-form initial-form)
        (remaining-forms forms))
    (loop while remaining-forms do
      (let ((current-form (car remaining-forms)))
        (if (listp current-form)
            (setf output-form (cons (car current-form)
                                    (append (cdr current-form) (list output-form))))
            (setf output-form (list current-form output-form))))
      (setf remaining-forms (cdr remaining-forms)))
    output-form))

(defmacro @= (place expr)
  "destructively set the result of expr into place"
  (multiple-value-bind (temps exprs stores store-expr access-expr)
      (get-setf-expansion place)
    `(let* (,@(mapcar #'list temps exprs)
            (,(car stores) (let ((% ,access-expr))
                             (declare (ignorable %))
                             ,expr)))
       ,store-expr)))

(defmacro mac (expr)
  `(pprint (macroexpand-1 ',expr)))

(defmacro compose (fn-name &rest args)
  (labels ((rec1 (args)
	     (if (= (length args) 1)
		 `(funcall ,@args x)
		 `(funcall ,(first args) ,(rec1 (rest args))))))
    `(defun ,fn-name (x) ,(rec1 args))))

;; from alexandria
(deftype string-designator () `(or symbol string character))
(defmacro with-gensyms (names &body forms)
  "binds each variable named by a symbol in names to a unique symbol around forms."
  `(let ,(mapcar (lambda (name)
                   (multiple-value-bind (symbol string)
                       (etypecase name
                         (symbol (values name (symbol-name name)))
                         ((cons symbol (cons string-designator null)) (values (car name)
                                                                              (string (cadr name)))))
                     `(,symbol (gensym ,string))))
          names)
     ,@forms))

;; from stivelosh.com
(defmacro gathering (&body body)
  "run body to gather some things and return a fresh list of them."
  (with-gensyms (result)
    `(let ((,result nil))
       (flet ((gather (item)
                (push item ,result)
                item))
         ,@body)
       (nreverse ,result))))

(defmacro when-let (bindings &body body)
  "bind bindings in parallel and execute body, short-circuiting on nil."
  (with-gensyms (block)
    `(block ,block
       (let (,@ (loop :for (symbol value) :in bindings
                      :collect `(,symbol (or ,value (return-from ,block)))))
         ,@body))))

(defmacro when-let* (bindings &body body)
  "bind bindings sequentially and execute body, short-circuiting on nil."
  (with-gensyms (block)
    `(block ,block
       (let* (,@ (loop :for (symbol value) :in bindings
                       :collect `(,symbol (or ,value (return-from ,block)))))
         ,@body))))

;; from alexandria
(defun parse-body (body &key documentation whole)
  "parses body into (values remaining-forms declarations doc-string)."
  (let ((doc nil)
        (decls nil)
        (current nil))
    (tagbody
     :declarations
       (setf current (car body))
       (when (and documentation (stringp current) (cdr body))
         (if doc
             (error "too many documentation strings in ~S." (or whole body))
             (setf doc (pop body)))
         (go :declarations)))
    (values body (nreverse decls) doc)))

(defmacro if-let (bindings &body body)
  "bind bindings in parallel and execute then if all are true or else otherwise."
  (with-gensyms (outer inner)
    (multiple-value-bind (body declarations) (parse-body body)
      (destructuring-bind (then else) body
        `(block ,outer
           (block ,inner
             (let ,(loop :for (symbol value) :in bindings
                         :collect `(,symbol (or ,value (return-from ,inner))))
               ,@declarations
               (return-from ,outer ,then)))
           ,else)))))

(defmacro if-let* (bindings &body body)
  "bind bindings sequentially and execute then if all are true or else otherwise."
  (with-gensyms (outer inner)
    (multiple-value-bind (body declarations) (parse-body body)
      (destructuring-bind (then else) body
        `(block ,outer
           (block ,inner
             (let* ,(loop :for (symbol value) :in bindings
                          :collect `(,symbol (or ,value (return-from ,inner))))
               ,@declarations
               (return-from ,outer ,then)))
           ,else)))))

(defmacro -<> (expr &rest forms)
  "threading macro with <> as a placeholder."
  `(let* ((<> ,expr)
          ,@(mapcar (lambda (form)
                      (if (symbolp form)
                          `(<> (,form <>))
                          `(<> ,form)))
                    forms))
     <>))
