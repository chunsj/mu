(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

(defun sum (xs) (when xs (reduce #'+ xs)))
(defun cumsum (lst) (loop :for x :in lst :sum x :into y :collect y))

(defun mean (xs) (when xs (/ (reduce #'+ xs) (length xs))))

(defun var (xs)
  "variance"
  (let* ((n (length xs))
         (m (/ (reduce #'+ xs) n)))
    (/ (reduce #'+ (map (type-of xs) (lambda (x)
                                       (expt (abs (- x m)) 2))
                        xs))
       (1- n))))

(defun sd (xs) (sqrt (var xs)))

(defun percentile (sequence percent)
  (let* ((sorted-vect (coerce (sort (copy-seq sequence) #'<) 'simple-vector))
         (n (length sorted-vect))
         (k (* n (/ percent 100)))
         (floor-k (floor k)))
    (if (= k floor-k)
        (/ (+ (aref sorted-vect k)
              (aref sorted-vect (1- k)))
           2)
        (aref sorted-vect floor-k))))

(defun median (sequence) (percentile sequence 50))

(defun min* (xs)
  (loop :for v :in xs
        :minimize v :into min
        :finally (return min)))

(defun max* (xs)
  (loop :for v :in xs
        :maximize v :into max
        :finally (return max)))

(defun mdd (vs &optional only)
  (let ((drawdowns nil)
        (max (coerce (max* vs) 'single-float))
        (min (coerce (min* vs) 'single-float))
        (max-so-far (car vs)))
    (loop :for v :in vs
          :for i :from 0
          :do (if (> v max-so-far)
                  (let ((drawdown 0))
                    (push drawdown drawdowns)
                    (setf max-so-far v))
                  (let ((drawdown (- max-so-far v)))
                    (push drawdown drawdowns))))
    (if only
        (coerce (max* drawdowns) 'single-float)
        (list :mdd (coerce (max* drawdowns) 'single-float)
              :min min
              :max max))))

(defun quantile (xs &optional (qlist '(2.5 25 50 75 97.5)))
  (let ((n ($count xs))
        (qlist (or qlist '(2.5 25 50 75 97.5))))
    (when (> n 5)
      (let ((vs (tx/sort #'< xs)))
        (loop :for q :in qlist
              :for ridx = (round (* n (/ q 100)))
              :collect (let ((i ridx))
                         (when (< i 0) (setf i 0))
                         (when (> i (1- n)) (setf i (1- n)))
                         (cons q ($ vs i))))))))

(defun gmean (sequence &optional (base 10))
  (expt base (mean (map 'list (lambda (x) (log x base)) sequence))))

(defun gmean1p (vs)
  (- (->> (tx/map (lambda (v) (log (1+ v))) vs)
          (mean)
          (exp))
     1.0))

(defun hmean (seq)
  "See: http://mathworld.wolfram.com/HarmonicMean.html"
  (/ (float (length seq))
     (loop :for n :in seq
           :sum (/ 1.0 n))))

(defun cv (sequence)
  "coefficient of variation"
  (* 100 (/ (sd sequence) (mean sequence))))

(defun se (sequence)
  "standard error"
  (/ (sd sequence) (sqrt (length sequence))))

(defun mode (sequence)
  "Returns two values: a list of the modes and the number of times
they occur."
  (let ((count-table (make-hash-table))
        mode (mode-count 0))
    (map nil (lambda (elt) (incf (gethash elt count-table 0))) sequence)
    (maphash (lambda (key value)
               (when (> value mode-count)
                 (setf mode key
                       mode-count value)))
             count-table)
    (list :model mode :n mode-count)))

(defun vrange (sequence)
  (- (reduce #'max sequence) (reduce #'min sequence)))

(defun stats/basic (sequence)
  "A combined calculation that is often useful.  Takes a sequence and
returns three values: mean, standard deviation and N."
  (list :mean (mean sequence) :sd (sd sequence) :n ($count sequence)))

(defun stats/summary (vs &optional verbose)
  (let ((qs (quantile vs))
        (min (min* vs))
        (max (max* vs))
        (mean (mean vs))
        (sd (sd vs)))
    (when verbose
      (prn "")
      (prn "** SUMMARY STATISTICS **")
      (prn "+-------+------------------------+")
      (prn "|       |                  VALUE |")
      (prn "|-------+------------------------+")
      (prn (format nil "|MIN    | ~22,6F |" min))
      (prn "|-------+------------------------+")
      (prn (format nil "|2.5%   | ~22,6F |" (cdr ($0 qs))))
      (prn "|-------+------------------------+")
      (prn (format nil "|25%    | ~22,6F |" (cdr ($1 qs))))
      (prn "|-------+------------------------+")
      (prn (format nil "|MEDIAN*| ~22,6F |" (cdr ($2 qs))))
      (prn "|-------+------------------------+")
      (prn (format nil "|75%    | ~22,6F |" (cdr ($3 qs))))
      (prn "|-------+------------------------+")
      (prn (format nil "|97.5%  | ~22,6F |" (cdr ($4 qs))))
      (prn "|-------+------------------------+")
      (prn (format nil "|MAX    | ~22,6F |" max))
      (prn "|=======+========================+")
      (prn (format nil "|MEAN*  | ~22,6F |" mean))
      (prn "|-------+------------------------+")
      (prn (format nil "|SD     | ~22,6F |" sd))
      (prn "|-------+------------------------+")
      (prn ""))
    (list :min min
          :P2.5 (cdr ($0 qs))
          :P25 (cdr ($1 qs))
          :median (cdr ($2 qs))
          :P75 (cdr ($3 qs))
          :P97.5 (cdr ($4 qs))
          :max max
          :mean mean
          :sd sd)))

(defun rsk (lst) (coerce (sd lst) 'single-float))

(defun stats/return (vs)
  (let ((mean (floor (mean vs)))
        (min (floor (min* vs)))
        (max (floor (max* vs)))
        (vol (rsk vs))
        (mddx (mdd (cumsum vs)))
        (ntrials ($count vs))
        (wins (->> (tx/filter (lambda (v) (>= v 0)) vs)
                   ($count))))
    (list :r.avg mean
          :r.min min
          :r.max max
          :vol (ceiling vol)
          :mdd (ceiling ($ mddx :mdd))
          :min (floor ($ mddx :min))
          :max (floor ($ mddx :max))
          :wr (floor (/ (* 100 wins) ntrials))
          :sr (if (> vol 0)
                  (floor (/ (* 100 mean) vol))
                  (round 1E6))
          :nr ntrials)))

(defun linspace (a b &optional (n 101))
  (let ((n (max n 2)))
    (let ((d (/ (- b a) (- n 1))))
      (loop :for i :from 0 :below n
            :for v = (+ a (* d i))
            :collect (if (= i (- n 1))
                         b
                         v)))))

;; CONDITIONAL AVERAGES
(defun stats/cavg (betas)
  (let ((xs (linspace 0.0 1.0 101)))
    (loop :for i :from 0 :below ($count xs)
          :for x = ($ xs i)
          :for y = (->> (loop :for beta :in betas
                              :for b = ($0 beta)
                              :for e = ($1 beta)
                              :for y = (+ (* x b) e)
                              :collect y)
                        (mean))
          :collect (list (1+ (round (* 100 x))) y))))

;; STATISTICAL TEST
(defun stats/t-test (vs1 vs2)
  (let ((n1 ($count vs1))
        (n2 ($count vs2))
        (m1 (mean vs1))
        (m2 (mean vs2))
        (s1 (sd vs1))
        (s2 (sd vs2)))
    (mu.stats::t-test-two-sample m1 s1 n1 m2 s2 n2 :variances-equal? nil)))

;; CONVERT A RATE INTO PERCENT VALUE
(defun ipct (v) (floor (/ (* v 10000) 100.0)))

(defun mean/ipct (vs) (ipct (mean vs)))
(defun sd/ipct (vs) (ipct (sd vs)))

;; HISTOGRAM RELATED
(defun histogram/ptp (xs) (- (apply #'max xs) (apply #'min xs)))

(defun width/sqrt (xs)
  (/ (histogram/ptp xs) (sqrt ($count xs))))
(defun width/sturges (xs)
  (/ (histogram/ptp xs) (+ (log ($count xs) 2) 1.0)))
(defun width/rice (xs)
  (/ (histogram/ptp xs) (* 2.0 (expt ($count xs) (/ 1.0 3.0)))))
(defun width/scott (xs)
  (* (expt (/ (* 24.0 (expt pi 0.5)) ($count xs)) (/ 1.0 3.0)) (sd xs)))
(defun width/doane (xs)
  (let ((n ($count xs)))
    (if (> n 2)
        (let ((sg1 (sqrt (/ (* 6.0 (- n 2)) (* (+ n 1.0) (+ n 3.0)))))
              (sigma (sd xs))
              (mx (mean xs)))
          (if (> sigma 0.0)
              (let* ((temp (mapcar (lambda (x) (expt (/ (- x mx) sigma) 3.0)) xs))
                     (g1 (mean temp))
                     (rg (+ 1.0 (/ (abs g1) sg1))))
                (/ (histogram/ptp xs) (+ 1.0 (log n 2) (log rg 2))))
              0.0))
        0.0)))
(defun width/fd (xs)
  (let ((q25 (percentile xs 25))
        (q75 (percentile xs 75)))
    (* 2.0 (- q75 q25) (expt ($count xs) (/ -1.0 3.0)))))
(defun width/auto (xs)
  (let ((fd (width/fd xs))
        (sturge (width/sturges xs)))
    (if (> fd 0)
        (min fd sturge)
        sturge)))

(defun histogram/outer-edges (xs &optional range)
  (let ((fe (if range (car range) (apply #'min xs)))
        (le (if range (cdr range) (apply #'max xs))))
    (if (= fe le)
        (cons (- fe 0.5) (+ le 0.5))
        (cons fe le))))

(defun histogram (xs &optional nbins)
  (let* ((edges (histogram/outer-edges xs))
         (wbin (if nbins
                   (/ (- (cdr edges) (car edges)) nbins)
                   (width/auto xs)))
         (nbin (or nbins (ceiling (/ (- (cdr edges) (car edges)) wbin))))
         (min (car edges))
         (pos 0)
         (data (sort (copy-list xs) #'<)))
    (loop :for i :from 0 :to nbin
          :for s = (+ min (* i wbin))
          :for e = (+ min (* (1+ i) wbin))
          :collect (let ((n 0))
                     (loop :for j :from pos :below ($count data)
                           :for d = ($ data j)
                           :while (and (>= d s) (< d e))
                           :do (incf n))
                     (incf pos n)
                     (cons s n)))))
