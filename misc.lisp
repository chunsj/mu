(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

(defun strcat (&rest args) (apply #'concatenate 'string args))

(defun strkw (str) (intern (string-upcase str) :keyword))

(defun split (separator string)
  (block ()
    (let ((list nil)
          (words 0)
          (end (length string))
          (separator (if (listp separator) separator (list separator))))
      (when (zerop end) (return nil))
      (flet ((separatorp (char) (find char separator))
             (done () (return (cons (subseq string 0 end) list))))
        (loop
          :for start = (position-if #'separatorp string :end end :from-end t)
          :do (when (null start) (done))
              (push (subseq string (1+ start) end) list)
              (incf words)
              (setf end start))))))

(defun parse-float (string) (with-input-from-string (in string) (float (read in))))

(defun strim (str)
  (string-trim '(#\Space #\Tab #\Newline #\Return #\Backspace #\Linefeed #\Page #\Rubout #\Ideographic_Space) str))

(defun to-integer (string)
  (handler-case
      (let ((s (strim string)))
        (if (and s (> (length s) 0))
            (parse-integer string)
            0))
    (condition (gex)
      (format T "TO-INTEGER: ~A" gex)
      ;;#+sbcl (sb-debug:backtrace)
      0)))

(defun to-float (string)
  (handler-case
      (let ((s (strim string)))
        (if (and s (> (length s) 0))
            (parse-float string)
            0))
    (condition (gex)
      (format T "TO-FLOAT: ~A" gex)
      ;;#+sbcl (sb-debug:backtrace)
      0.0)))

(defun read-lines-from (filename &key (txfn (lambda (line) line)))
  (when (probe-file filename)
    (with-open-file (stream filename :direction :input)
      (loop :for line = (read-line stream nil) :while line :collect (funcall txfn line)))))

(defun hash-table-keys (ht)
  (loop :for key :being :the :hash-keys :of ht :collect key))

(defun hash-table-values (ht)
  (loop :for key :being :the :hash-keys :of ht :collect (gethash key ht)))

(defun hash-table-pairs (ht)
  (loop :for key :being :the :hash-keys :of ht :collect (cons key (gethash key ht))))

(defun allp (list) (eval `(and ,@list)))
(defun somep (list) (eval `(or ,@list)))

(defmacro filter (fn seq) `(remove-if-not ,fn ,seq))

(defmacro distinct (seq) `(remove-duplicates ,seq :test #'equal))

(defun slurp (filename)
  (when (probe-file filename)
    (with-open-file (in filename)
      (loop :for line = (read-line in nil)
            :while line
            :collect line))))

;; (defun parse-csv-line (line)
;;   (let ((data nil)
;;         (datum nil)
;;         (inquote nil))
;;     (loop :for ch :across line
;;           :do (cond ((and (not inquote) (char= ch #\,))
;;                      (progn
;;                        (push (format nil "~{~A~}" (reverse datum)) data)
;;                        (setf datum nil)))
;;                     ((char= ch #\") (if inquote (setf inquote nil) (setf inquote T)))
;;                     (T (push ch datum))))
;;     (push (format nil "~{~A~}" (reverse datum)) data)
;;     (reverse data)))

(defun slurp/csv (filename &optional (separator #\,))
  (when (probe-file filename)
    (cl-csv:read-csv (pathname filename) :separator separator)))

(defun slurp/str (filename)
  (when (probe-file filename)
    (with-open-file (in filename)
      (with-output-to-string (s)
        (loop :for line = (read-line in nil)
              :while line
              :do (format s "~A" line))))))

(defun slurp/lsp (filename)
  (when-let ((str (slurp/str filename)))
    (read-from-string str)))

(defun parse/csv (string &optional (separator #\,))
  (cl-csv:read-csv string :separator separator))

(defun with-directory-filenames-collect (pathspec fn)
  (loop :for df :in (directory pathspec)
        :for fname = (namestring df)
        :collect (funcall fn fname)))

(defun with-directory-filenames-do (pathspec fn)
  (loop :for df :in (directory pathspec)
        :for fname = (namestring df)
        :do (funcall fn fname)))

(defmethod $count ((s LIST)) (length s))
(defmethod $count ((s ARRAY)) (length s))
(defmethod $count ((s HASH-TABLE)) (hash-table-count s))

(defun copy-hash-table (hash-table)
  (let ((ht (make-hash-table
             :test (hash-table-test hash-table)
             :rehash-size (hash-table-rehash-size hash-table)
             :rehash-threshold (hash-table-rehash-threshold hash-table)
             :size (hash-table-size hash-table))))
    (loop :for key :being :each :hash-key :of hash-table :using (hash-value value)
          :do (setf (gethash key ht) value)
          :finally (return ht))))

(defun repeat (n data-or-function)
  (cond ((functionp data-or-function) (loop :for i :from 0 :below n
                                            :collect (funcall data-or-function)))
        (t (loop :for i :from 0 :below n :collect data-or-function))))

(defun xrange (low high &optional (step-size 1))
  (loop :for i :from low :below high :by step-size :collect i))

(defun gc ()
  #+sbcl (sb-ext::gc)
  #+ccl (ccl:gc))

(defun gcf ()
  #+sbcl (sb-ext::gc :full t)
  #+ccl (ccl:gc))

(defun prns (s)
  (format *standard-output* "~A" s)
  (finish-output *standard-output*))

(defun prn (&rest objects)
  (handler-case
      (progn
        (format *standard-output* "~%~{~A~^ ~}" objects)
        (finish-output *standard-output*))
    (condition (gex)
      (declare (ignore gex))
      ;; don't know what to do
      #+sbcl (sb-debug:print-backtrace)
      #+sbcl (sb-ext:exit :code -1 :abort T :timeout 1)
      #+ccl (ccl:quit))))

(defparameter *mu-display-warning* nil)

(defun wrn/enable () (setf *mu-display-warning* T))
(defun wrn/disable () (setf *mu-display-warning* nil))

(defun wrn (&rest objects)
  (when *mu-display-warning*
    (handler-case
        (progn
          (format *standard-output* "~%~{~A~^ ~}" objects)
          (finish-output *standard-output*))
      (condition (gex)
        (declare (ignore gex))
        ;; don't know what to do
        #+sbcl (sb-debug:print-backtrace)
        #+sbcl (sb-ext:exit :code -1 :abort T :timeout 1)
        #+ccl (ccl:quit)))))

(defun rotate-left (n l)
  (append (nthcdr n l) (butlast l (- (length l) n))))

(defun rotate-right (n l)
  (rotate-left (- (length l) n) l))

(defun rotate-left-string (n string)
  (coerce (rotate-left n (coerce string 'list)) 'string))

(defun rotate-right-string (n string)
  (coerce (rotate-right n (coerce string 'list)) 'string))

(defun flatten (obj)
  (do* ((result (list obj))
        (node result))
       ((null node) (delete nil result))
    (cond ((consp (car node))
           (when (cdar node) (push (cdar node) (cdr node)))
           (setf (car node) (caar node)))
          (t (setf node (cdr node))))))

(defun re/matches (regexp string)
  (cl-ppcre:all-matches-as-strings regexp string))

(defun re/replace (regexp string replacement)
  (cl-ppcre:regex-replace-all regexp string replacement))

(defun interleave (lst1 lst2)
  (loop :with ret
        :for l1 :in lst1
        :for l2 :in lst2
        :do (progn (push l1 ret)
                   (push l2 ret))
        :finally (return (nreverse ret))))

(defun parse/integer (string)
  (when (and string (> ($count string) 0))
    (let ((s (strim string)))
      (when (and (> ($count s) 0)
                 (not (string-equal "-" s))
                 (not (string-equal "N/A" s))
                 (not (string-equal "NA" s))
                 (not (string-equal "NIL" s)))
        (parse-integer (substitute #\- #\△ (remove #\, s)) :junk-allowed T)))))

(defun parse/float (string)
  (when (and string (> ($count string) 0))
    (let ((s (strim string)))
      (when (and (> ($count s) 0)
                 (not (string-equal "-" s))
                 (not (string-equal "N/A" s))
                 (not (string-equal "NA" s))
                 (not (string-equal "NIL" s)))
        (parse-float (substitute #\- #\△ (remove #\, s)))))))
