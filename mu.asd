(defsystem mu
  :name "mu"
  :author "Sungjin Chun <chunsj@gmail.com>"
  :version "25.030"
  :maintainer "Sungjin Chun <chunsj@gmail.com>"
  :license "GPL3"
  :description "my support utilities"
  :long-description "my own macros and utilities"
  :depends-on ("cl-csv"
               "cl-ppcre"
               "cl-cpus"
               "alexandria"
               "lparallel")
  :components ((:file "package")
               (:file "macros")
               (:file "accessors")
               (:file "seq")
               (:file "math")
               (:file "tx")
               (:file "tx.ext")
               (:file "misc")
               (:file "table")
               (:file "stats")
               (:file "amb")
               (:file "df")
               (:file "mm")
               (:file "dz")
               (:file "load")))
