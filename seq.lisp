(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

(defgeneric $conj (coll x &rest xs))

(defmethod $conj ((coll list) x &rest xs)
  (if x
      (if (null xs)
          (cons x coll)
          (apply #'$conj (cons x coll) (car xs) (cdr xs)))
      coll))

(defmethod $conj ((coll array) x &rest xs)
  (if x
      (if (null xs)
          (concatenate 'vector coll #[x])
          (apply #'$conj (concatenate 'vector coll #[x]) (car xs) (cdr xs)))
      coll))

(defun hash-table-copy (coll)
  (let ((nhtbl (make-hash-table :test (hash-table-test coll))))
    (loop :for key :being :the :hash-keys :of coll
          :do (setf ($ nhtbl key) ($ coll key)))
    nhtbl))

(defun hash-table-merge (coll x &rest xs)
  (if x
      (if (null xs)
          (let ((nhtbl coll))
            (loop :for key :being :the :hash-keys :of x
                  :do (setf ($ nhtbl key) ($ x key)))
            nhtbl)
          (apply #'$conj
                 (let ((nhtbl coll))
                   (loop :for key :being :the :hash-keys :of x
                         :do (setf ($ nhtbl key) ($ x key)))
                   nhtbl)
                 (car xs)
                 (cdr xs)))
      coll))

(defmethod $conj ((coll hash-table) x &rest xs)
  (apply #'hash-table-merge (hash-table-copy coll) x xs))

(defgeneric $concat (coll xs &rest others))

(defmethod $concat ((coll list) xs &rest others)
  (apply #'concatenate 'list coll xs others))

(defmethod $concat ((coll array) xs &rest others)
  (apply #'concatenate 'vector coll xs others))

(defmethod $concat ((coll hash-table) xs &rest others)
  (apply #'$conj coll xs others))

(defmethod $concat ((coll string) xs &rest others)
  (apply #'concatenate 'string coll xs others))

(defun list/getp (list index) (getf list index))
(defun list/getf (list index) (getf list index))
(defun list/geta (list index) (cdr (assoc index list)))
