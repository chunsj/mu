(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu.stats)

(defun mean (xs) (/ (reduce #'+ xs) (length xs)))

(defun geometric-mean (sequence &optional (base 10))
  (expt base (mean (map 'list (lambda (x) (log x base)) sequence))))

(defun harmonic-mean (seq)
  "See: http://mathworld.wolfram.com/HarmonicMean.html"
  (/ (float (length seq))
     (loop for n in seq
           sum (/ 1.0 n))))

(defun cross-mean (l &aux k r)
  "Cross mean takes a list of lists, as ((1 2 3) (4 3 2 1) ...) and
produces a list with mean and standard error for each VERTICLE entry,
so, as: ((2.5 . 1) ...) where the first pair is computed from the nth
1 of all the sublists in the input set, etc.  This is useful in some
cases of data cruching.
Note that missing data is assumed to be always at the END of lists.
If it isn't, you've got to do something previously to interpolate."
  (let* ((nmax (reduce #'max (mapcar #'length l)))
	 (vs (make-array nmax)))
    (dolist (i l)
      (setq k 0)
      (dolist (v i)
	(push v (aref vs k))
	(incf k)))
    (dotimes (i nmax)
      (push (cons (mean (aref vs i))
		  (standard-error (aref vs i)))
	    r))
    (reverse r)))

(defun lmean (ll)
  "Lmean takes the mean of entries in a list of lists vertically.
So: (lmean '((1 2) (5 6))) -> (3 4) The args have to be the same
length."
  (let* ((n (length ll))
	 (sums (copy-list (pop ll))) ; copy so that incf doesn't wreck us!
	 (len (length sums)))
    (dolist (l ll)
      (dotimes (k len)
	(incf (nth k sums) (nth k l))
	))
    (setq n (float n))
    (mapcar (lambda (v) (/ v n)) sums)))

(defun variance (xs)
  (let* ((n (length xs))
         (m (/ (reduce #'+ xs) n)))
    (/ (reduce #'+ (mapcar (lambda (x)
                             (expt (abs (- x m)) 2))
                           xs))
       (1- n))))

(defun sd (xs) (sqrt (variance xs)))
(defun standard-deviation (xs) (sd xs))

(defun mean-sd-n (sequence)
  "A combined calculation that is often useful.  Takes a sequence and
returns three values: mean, standard deviation and N."
  (values (mean sequence) (standard-deviation sequence) (length sequence)))

(defun mode (sequence)
  "Returns two values: a list of the modes and the number of times
they occur."
  (let ((count-table (make-hash-table))
        mode (mode-count 0))
    (map nil (lambda (elt) (incf (gethash elt count-table 0))) sequence)
    (maphash (lambda (key value)
               (when (> value mode-count)
                 (setf mode key
                       mode-count value)))
             count-table)
    (values mode mode-count)))

(defun vrange (sequence)
  (- (reduce #'max sequence) (reduce #'min sequence)))

(defun percentile (sequence percent)
  (let* ((sorted-vect (coerce (sort (copy-seq sequence) #'<) 'simple-vector))
         (n (length sorted-vect))
         (k (* n (/ percent 100)))
         (floor-k (floor k)))
    (if (= k floor-k)
        (/ (+ (aref sorted-vect k)
              (aref sorted-vect (1- k)))
           2)
        (aref sorted-vect floor-k))))

(defun median (sequence)
  (test-variables (sequence :numseq))
  (percentile sequence 50))

(defun coefficient-of-variation (sequence)
  (* 100 (/ (standard-deviation sequence) (mean sequence))))

(defun standard-error-of-the-mean (sequence)
  (/ (standard-deviation sequence) (sqrt (length sequence))))

(defun standard-error (sequence)
  (standard-error-of-the-mean sequence))

(defun factorial (n)
  (loop :for result = 1 :then (* result i)
        :for i :from 2 :to n
        :finally (return result)))

(defun average-rank (value sorted-values)
  "Average rank calculation for non-parametric tests.  Ranks are 1
based, but lisp is 0 based, so add 1!"
  (let ((first (position value sorted-values))
        (last (position value sorted-values :from-end t)))
    (1+ (if (= first last)
            first
            (/ (+ first last) 2)))))

(defun fisher-z-transform (r)
  "Transforms the correlation coefficient to an approximately normal
distribution."
  (* 1/2 (log (/ (1+ r) (- 1 r)))))

(defun permutations (n k)
  "How many ways to take n things taken k at a time, when order
matters"
  (let ((p 1))
    (dotimes (i (1+ k) p)
      (setq p (* p (- n i))))))

(defun choose (n k)
  "How may ways to take n things taken k at a time, when order doesn't
matter"
  (/ (factorial n) (* (factorial k) (factorial (- n k)))))

(defun bin-and-count (sequence n)
  "Make N equal width bins and count the number of elements of sequence
that belong in each."
  (let* ((min  (reduce #'min sequence))
         (increment (/ (- (reduce #'max sequence) min) n))
         (bins (make-array n :initial-element 0)))
    (dotimes (bin n bins)
      (setf (aref bins bin)
            (count-if (lambda (x) (and (>= x (+ min (* bin increment)))
                                  (< x (+ min (* (1+ bin) increment)))))
                      sequence)))))

(defmacro square (x) `(* ,x ,x))

(defun sign (x)
  (cond ((minusp x) -1)
        ((plusp x) 1)
        ((zerop x) 0)
        (t nil)))

(defun round-float (x &key (precision 5))
  "Rounds a floating point number to a specified number of digits
precision."
  (/ (round x (expt 10 (- precision))) (expt 10 precision)))

(defun round-up (x)
  (multiple-value-bind (rounded ignore) (ceiling x)
    (declare (ignore ignore))
    rounded))

(defun sum (l &aux (sum 0))
  (dolist (n l) (incf sum n)) sum)

(defun sqr (a)
  (if (numberp a) (expt a 2)
      (mapcar #'* a a)))

(defun all-squares (as bs &aux squares)
  (dolist (a as)
    (dolist (b bs)
      (push (sqr (* a b)) squares))))

(defun max* (l &rest ll &aux m)
  (if ll (setq l (cons l ll)))
  (setq m (pop l))
  (dolist (i l)
    (if (> i m) (setq m i)))
  m)

(defun min* (l &rest ll &aux m)
  (if ll (setq l (cons l ll)))
  (setq m (pop l))
  (dolist (i l)
    (if (< i m) (setq m i)))
  m)

(defmacro underflow-goes-to-zero (&body body)
  "Protects against floating point underflow errors and sets the value to 0.0 instead."
  `(handler-case
       (progn ,@body)
     (floating-point-underflow (condition)
       (declare (ignore condition))
       (values 0.0d0))))

(defun safe-exp (x)
  "Eliminates floating point underflow for the exponential function.
Instead, it just returns 0.0d0"
  (setf x (coerce x 'double-float))
  (if (< x (log least-positive-double-float))
      0.0d0
      (exp x)))

(defun gamma-ln (x)
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (cond ((<= x 0) (error "arg to gamma-ln must be positive:  ~s" x))
	((> x 1.0d302)
         (error "Argument too large:  ~e" x))
	((= x 0.5d0)
         ;; special case for this arg, since it is used by the error-function
         (log (sqrt pi)))
	((< x 1)
         ;; Use reflection formula:  Gamma(1-z) = z*pi/(Gamma(1+z)sin(pi*z))
         (let ((z (- 1.0d0 x)))
           (- (+ (log z) (log pi)) (+ (gamma-ln (+ 1.0 z)) (log (sin (* pi z)))))))
	(t (let* ((xx  (- x 1.0d0))
                  (tmp (+ xx 5.5d0))
                  (ser 1.0d0))
             (declare (type double-float xx tmp ser))
             (decf tmp (* (+ xx 0.5d0) (log tmp)))
             (dolist (coef '(76.18009173d0 -86.50532033d0 24.01409822d0
                             -1.231739516d0 0.120858003d-2 -0.536382d-5))
               (declare (type double-float coef))
               (incf xx 1.0d0)
               (incf ser (/ coef xx)))
             (- (log (* 2.50662827465d0 ser)) tmp)))))

(defun gamma-incomplete (a x)
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (declare (optimize (safety 3)))
  (setq a (coerce a 'double-float))
  (let ((gln (the double-float (gamma-ln a))))
    (when (= x 0.0)
      (return-from gamma-incomplete (values 0.0d0 gln)))
    (if (< x (+ a 1.0d0))
        ;; Use series representation.  The following is the code of what
        ;; Numerical Recipes in C calls ``GSER'
        (let* ((itmax 1000)
               (eps   3.0d-7)
               (ap    a)
               (sum   (/ 1d0 a))
               (del sum))
          (declare (type double-float ap sum del))
          (dotimes (i itmax)
            (incf ap 1.0d0)
            (setf del (* del (/ x ap)))
            (incf sum del)
            (if (< (abs del) (* eps (abs sum)))
                (let ((result (underflow-goes-to-zero
                                (* sum (safe-exp (- (* a (log x)) x gln))))))
                  (return-from gamma-incomplete (values result gln)))))
          (error "Series didn't converge:~%~
                  Either a=~s is too large, or ITMAX=~d is too small." a itmax))
        ;; Use the continued fraction representation.  The following is the
        ;; code of what Numerical Recipes in C calls ``GCF.'' Their code
        ;; computes the complement of the desired result, so we subtract from
        ;; 1.0 at the end.
        (let ((itmax 1000)
              (eps   3.0e-7)
              (gold 0d0) (g 0d0) (fac 1d0) (b1 1d0) (b0 0d0)
              (anf 0d0) (ana 0d0) (an 0d0) (a1 x) (a0 1d0))
          (declare (type double-float gold g fac b1 b0 anf ana an a1 a0))
          (dotimes (i itmax)
            (setf an  (coerce (1+ i) 'double-float)
                  ana (- an a)
                  a0  (* fac (+ a1 (* a0 ana)))
                  b0  (* fac (+ b1 (* b0 ana)))
                  anf (* fac an)
                  a1  (+ (* x a0) (* anf a1))
                  b1  (+ (* x b0) (* anf b1)))
            (unless (zerop a1)
              (setf fac (/ 1.0d0 a1)
                    g   (* b1 fac))
              (if (< (abs (/ (- g gold) g)) eps)
                  (let ((result (underflow-goes-to-zero
                                  (* (safe-exp (- (* a (log x)) x gln)) g))))
                    (return-from
                     gamma-incomplete (values (- 1.0 result) gln)))
                  (setf gold g))))
          (error "Continued Fraction didn't converge:~%~
                  Either a=~s is too large, or ITMAX=~d is too small." a
                  itmax)))))

(defun beta-incomplete (a b x)
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (flet ((betacf (a b x)
           ;; straight from Numerical Recipes in C, section 6.3
           (declare (type double-float a b x))
           (let ((ITMAX 1000)
                 (EPS   3.0d-7)
                 (qap 0d0) (qam 0d0) (qab 0d0) (em  0d0) (tem 0d0) (d 0d0)
                 (bz  0d0) (bm  1d0) (bp  0d0) (bpp 0d0)
                 (az  1d0) (am  1d0) (ap  0d0) (app 0d0) (aold 0d0))
             (declare (type double-float qap qam qab em tem d
                            bz bm bp bpp az am ap app aold))
             (setf qab (+ a b)
                   qap (+ a 1d0)
                   qam (- a 1d0)
                   bz  (- 1d0 (/ (* qab x) qap)))
             (dotimes (m ITMAX)
               (setf em   (coerce (1+ m) 'double-float)
                     tem  (+ em em)
                     d    (/ (* em (- b em) x)
                             (* (+ qam tem) (+ a tem)))
                     ap   (+ az (* d am))
                     bp   (+ bz (* d bm))
                     d    (/ (* (- (+ a em)) (+ qab em) x)
                             (* (+ qap tem) (+ a tem)))
                     app  (+ ap (* d az))
                     bpp  (+ bp (* d bz))
                     aold az
                     am   (/ ap bpp)
                     bm   (/ bp bpp)
                     az   (/ app bpp)
                     bz   1d0)
               (if (< (abs (- az aold)) (* EPS (abs az)))
                   (return-from betacf az)))
             (error "a=~s or b=~s too big, or ITMAX too small in BETACF"
                    a b))))
    (declare (notinline betacf))
    (setq a (coerce a 'double-float) b (coerce b 'double-float)
          x (coerce x 'double-float))
    (when (or (< x 0d0) (> x 1d0))
      (error "x must be between 0d0 and 1d0:  ~f" x))
    ;; bt is the factors in front of the continued fraction
    (let ((bt (if (or (= x 0d0) (= x 1d0))
                  0d0
                  (exp (+ (gamma-ln (+ a b))
                          (- (gamma-ln a))
                          (- (gamma-ln b))
                          (* a (log x))
                          (* b (log (- 1d0 x))))))))
      (if (< x (/ (+ a 1d0) (+ a b 2.0)))
          ;; use continued fraction directly
          (/ (* bt (betacf a b x)) a)
          ;; use continued fraction after making the symmetry transformation
          (- 1d0 (/ (* bt (betacf b a (- 1d0 x))) b))))))

(defun error-function (x)
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (let ((erf (gamma-incomplete .5 (square x))))
    (if (>= x 0.0) erf (- erf))))

(defun error-function-complement (x)
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (let* ((z (abs x))
         (y (/ 1d0 (+ 1d0 (* 0.5 z))))   ; instead of t
         (ans
           (* y (exp (+ (* (- z) z)
                        -1.26551223
                        (* y
                           (+ 1.00002368
                              (* y
                                 (+ 0.37409196
                                    (* y
                                       (+ 0.09678418
                                          (* y
                                             (+ -0.18628806
                                                (* y
                                                   (+ 0.27886807
                                                      (* y
                                                         (+ -1.13520398
                                                            (* y
                                                               (+ 1.48851587
                                                                  (* y
                                                                     (+ -0.82215223
                                                                        (* y 0.17087277))))))))))))))))))))))
    (declare (type double-float z y ans))
    (if (>= x 0.0)
	ans
        (- 2.0 ans))))

(defun find-critical-value
    (p-function p-value &optional (x-tolerance .00001) (y-tolerance .00001))
  "Adopted from CLASP 1.4.3, http://eksl-www.cs.umass.edu/clasp.html"
  (let* ((x-low 0d0)
         (fx-low 1d0)
         (x-high 1d0)
         (fx-high (coerce (funcall p-function x-high) 'double-float)))
    ;; double up
    (declare (type double-float x-low fx-low x-high fx-high))
    (do () (nil)
      ;; for general functions, we'd have to try the other way of bracketing,
      ;; and probably have another way to terminate if, say, y is not in the
      ;; range of f.
      (when (>= fx-low p-value fx-high)
	(return))
      (setf x-low x-high
            fx-low fx-high
            x-high (* 2.0 x-high)
            fx-high (funcall p-function x-high)))
    ;; binary search
    (do () (nil)
      (let* ((x-mid  (/ (+ x-low x-high) 2.0))
             (fx-mid (funcall p-function x-mid))
             (y-diff (abs (- fx-mid p-value)))
             (x-diff (- x-high x-low)))
	(when (or (< x-diff x-tolerance)
                  (< y-diff y-tolerance))
          (return-from find-critical-value x-mid))
	;; Because significance is monotonically decreasing with x, if the
	;; function is above the desired p-value...
	(if (< p-value fx-mid)
            ;; then the critical x is in the upper half
            (setf x-low x-mid
                  fx-low fx-mid)
            ;; otherwise, it's in the lower half
            (setf x-high x-mid
                  fx-high fx-mid))))))

(defun fisher-exact-test (contingency-table &key (tails :both))
  "Fisher's exact test.  Gives a p value for a particular 2x2
contingency table"
  (flet ((table-probability (a b c d)
           (let ((n (+ a b c d)))
             (/ (* (factorial (+ a b)) (factorial (+ c d))
                   (factorial (+ a c)) (factorial (+ b d)))
                (* (factorial n) (factorial a) (factorial b)
                   (factorial c) (factorial d))))))

    (let ((a (aref contingency-table 0 0))
          (b (aref contingency-table 0 1))
          (c (aref contingency-table 1 0))
          (d (aref contingency-table 1 1)))
      (let* ((row-margin1 (+ a b))
             (row-margin2 (+ c d))
             (column-margin1 (+ a c))
             (column-margin2 (+ b d))
             (n (+ a b c d))
             (table-probabilities
               (make-array (1+ (min row-margin1 row-margin2 column-margin1
                                    column-margin2)))))

        ;; rearrange so that the first row and column marginals are
        ;; smallest.  Only need to change first margins and a.

        (cond ((and (< row-margin2 row-margin1) (< column-margin2 column-margin1))
               (psetq a d
                      row-margin1 row-margin2
                      column-margin1 column-margin2))
              ((< row-margin2 row-margin1)
               (psetq a c
                      row-margin1 row-margin2))
              ((< column-margin2 column-margin1)
               (psetq a b
                      column-margin1 column-margin2)))
        (dotimes (i (length table-probabilities))
          (let* ((test-a i)
                 (test-b (- row-margin1 i))
                 (test-c (- column-margin1 i))
                 (test-d (- n (+ test-a test-b test-c))))
            (setf (aref table-probabilities i)
                  (table-probability test-a test-b test-c test-d))))
        (let ((above (reduce #'+ (subseq table-probabilities 0 (1+ a))))
              (below (reduce #'+ (subseq table-probabilities a))))
          (float
           (ecase tails
             ((:both) (* 2 (min above below)))
             ((:positive) below)
             ((:negative) above))
           1d0))))))

(defun poisson-probability (mu k)
  "Probability of seeing k events over a time period when the expected
number of events over that time is mu."
  (let ((mu (coerce mu 'double-float)))
    (/ (* (exp (- mu)) (expt mu k))
       (factorial k))))

(defun poisson-cumulative-probability (mu k)
  "Probability of seeing fewer than K events over a time period when
the expected number events over that time is mu."
  (if (< k 170)
      (let ((sum 0d0))
        (dotimes (x k sum)
          (incf sum (poisson-probability mu x))))
      (let ((mu (coerce mu 'double-float))
            (k (coerce k 'double-float)))
        (- 1d0 (gamma-incomplete k mu)))))

(defun poisson-ge-probability (mu x)
  "Probability of X or more events when expected is mu."
  (- 1 (poisson-cumulative-probability mu x)))

(defun poisson-mu-ci (x alpha)
  "Confidence interval for the Poisson parameter mu
Given x observations in a unit of time, what is the 1-alpha confidence
interval on the Poisson parameter mu (= lambda*T)?
Since find-critical-value assumes that the function is monotonic
increasing, adjust the value we are looking for taking advantage of
reflectiveness."
  (values
   (find-critical-value
    (lambda (mu) (poisson-cumulative-probability mu (1- x)))
    (- 1 (/ alpha 2)))
   (find-critical-value
    (lambda (mu) (poisson-cumulative-probability mu x))
    (/ alpha 2))))

(defun poisson-test-one-sample (observed mu &key (tails :both) (approximate? nil))
  "The significance of a one sample test for the equality of an observed
number of events (observed) and an expected number mu under the
poisson distribution.  Normal theory approximation is not that great,
so don't use it unless told."
  (if approximate?
      (let ((x-square (/ (square (- observed mu)) mu)))
        (- 1 (chi-square-cdf x-square 1)))
      (let ((probability-more-extreme
              (if (< observed mu)
                  (poisson-cumulative-probability mu observed)
                  (poisson-ge-probability mu observed))))
        (ecase tails
          ((:negative :positive) probability-more-extreme)
          (:both (min (* 2 probability-more-extreme) 1.0))))))

(defun binomial-probability (n k p)
  "P(X=k) for X a binomial random variable with parameters n &
p. Binomial expectations for seeing k events in N trials, each having
probability p.  Use the Poisson approximation if N>100 and P<0.01."
  (if (and (> n 100) (< p 0.01))
      (poisson-probability (* n p) k)
      (let ((p (coerce p 'double-float)))
        (* (choose n k)
           (expt p k)
           (expt (- 1 p) (- n k))))))

(defun binomial-cumulative-probability (n k p)
  "P(X<k) for X a binomial random variable with parameters n &
p. Bionomial expecations for fewer than k events in N trials, each
having probability p."
  (let ((sum-up-to-k-1 0d0))
    (dotimes (i k sum-up-to-k-1)
      (incf sum-up-to-k-1 (binomial-probability n i p)))))

(defun binomial-ge-probability (n k p)
  "The probability of k or more occurances in N events, each with
probability p."
  (- 1 (binomial-cumulative-probability n k p)))

(defun binomial-le-probability (n k p)
  (let ((sum-up-to-k 0d0))
    (dotimes (i (1+ k) sum-up-to-k)
      (incf sum-up-to-k (binomial-probability n i p)))))

(defun binomial-probability-ci (n p alpha &key exact?)
  "Confidence intervals on a binomial probability.  If a binomial
probability of p has been observed in N trials, what is the 1-alpha
confidence interval around p?  Approximate (using normal theory
approximation) when npq >= 10 unless told otherwise"
  (if (and (> (* n p (- 1 p)) 10) (not exact?))
      (let ((difference (* (z (- 1 (/ alpha 2)))
                           (sqrt (/ (* p (- 1 p)) n)))))
        (values (- p difference) (+ p difference)))
      (values (find-critical-value
               (lambda (p1) (binomial-cumulative-probability n (floor (* p n)) p1))
               (- 1 (/ alpha 2)))
              (find-critical-value
               (lambda (p2) (binomial-cumulative-probability n (1+ (floor (* p n))) p2))
               (/ alpha 2)))))

(defun binomial-test-one-sample (p-hat n p &key (tails :both) (exact? nil))
  "The significance of a one sample test for the equality of an observed
probability p-hat to an expected probability p under a binomial
distribution with N observations.  Use the normal theory approximation
if n*p*(1-p) > 10 (unless the exact flag is true)."
  (let ((q (- 1 p)))
    (if (and (> (* n p q) 10) (not exact?))
        (let ((z (/ (- p-hat p) (sqrt (/ (* p q) n)))))
          (ecase tails
            (:negative (phi z))
            (:positive (- 1 (phi z)))
            (:both (* 2 (if (<= p-hat p) (phi z) (- 1 (phi z)))))))
        (let* ((observed (round (* p-hat n)))
               (probability-more-extreme
                 (if (<= p-hat p)
                     (binomial-cumulative-probability n observed p)
                     (binomial-ge-probability n observed p))))
          (ecase tails
            ((:negative :positive) probability-more-extreme)
            (:both (min (* 2 probability-more-extreme) 1.0)))))))

(defun binomial-test-two-sample (p-hat1 n1 p-hat2 n2 &key (tails :both)
                                                       (exact? nil))
  "Are the observed probabilities of an event (p-hat1 and p-hat2) in
N1/N2 trials different? The normal theory method implemented here.
The exact test is Fisher's contingency table method, below."
  (let* ((p-hat (/ (+ (* p-hat1 n1) (* p-hat2 n2)) (+ n1 n2)))
         (q-hat (- 1 p-hat))
         (z (/ (- (abs (- p-hat1 p-hat2)) (+ (/ (* 2 n1)) (/ (* 2 n2))))
               (sqrt (* p-hat q-hat (+ (/ n1) (/ n2)))))))
    (if (and (> (* n1 p-hat q-hat) 5)
             (> (* n2 p-hat q-hat) 5)
             (not exact?))
        (* (- 1 (phi z)) (if (eql tails :both) 2 1))
        (let ((contingency-table (make-array '(2 2))))
          (setf (aref contingency-table 0 0) (* p-hat1 n1)
                (aref contingency-table 0 1) (- 1 (* p-hat1 n1))
                (aref contingency-table 1 0) (* p-hat2 n2)
                (aref contingency-table 1 1) (- 1 (* p-hat2 n2)))
          (fisher-exact-test contingency-table :tails tails)))))

(defun mcnemars-test (a-discordant-count b-discordant-count &key (exact? nil))
  "McNemar's test for correlated proportions, used for longitudinal
studies. Look only at the number of discordant pairs (one treatment is
effective and the other is not).  If the two treatments are A and B,
a-discordant-count is the number where A worked and B did not, and
b-discordant-count is the number where B worked and A did not."
  (let ((n (+ a-discordant-count b-discordant-count)))
    (if (and (> n 20) (not exact?))
        (let ((x2 (/ (square
                      (- (abs (- a-discordant-count b-discordant-count)) 1))
                     n)))
          (- 1 (chi-square-cdf x2 1)))
        (cond ((= a-discordant-count b-discordant-count) 1.0)
              ((< a-discordant-count b-discordant-count)
               (* 2 (binomial-le-probability n a-discordant-count 1/2)))
              (t (* 2 (binomial-ge-probability n a-discordant-count 1/2)))))))

(defun binomial-test-one-sample-sse (p-estimated p-null &key
                                                          (alpha 0.05) (1-beta 0.95)
                                                          (tails :both))
  "Returns the number of subjects needed to test whether an observed
probability is significantly different from a particular binomial null
hypothesis with a significance alpha and a power 1-beta."
  (let ((q-null (- 1 p-null))
        (q-estimated (- 1 p-estimated)))
    (round-up
     (/ (* p-null q-null
           (square (+ (z (- 1 (if (eql tails :both) (/ alpha 2) alpha)))
                      (* (z 1-beta)
                         (sqrt (/ (* p-estimated q-estimated)
                                  (* p-null q-null)))))))
        (square (- p-estimated p-null))))))

(defun binomial-test-two-sample-sse (p1 p2 &key (alpha 0.05)
                                             (sample-ratio 1)
                                             (1-beta .95) (tails :both))
  "The number of subjects needed to test if two binomial probabilities
are different at a given significance alpha and power 1-beta.  The
sample sizes can be unequal; the p2 sample is sample-sse-ratio * the
size of the p1 sample.  It can be a one tailed or two tailed test."
  (let* ((q1 (- 1 p1))
         (q2 (- 1 p2))
         (delta (abs (- p1 p2)))
         (p-bar (/ (+ p1 (* sample-ratio p2)) (1+ sample-ratio)))
         (q-bar (- 1 p-bar))
         (z-alpha (z (- 1 (if (eql tails :both) (/ alpha 2) alpha))))
         (z-beta (z 1-beta))
         (n1 (round-up
              (/ (square (+ (* (sqrt (* p-bar q-bar (1+ (/ sample-ratio))))
                               z-alpha)
                            (* (sqrt (+ (* p1 q1) (/ (* p2 q2) sample-ratio)))
                               z-beta)))
                 (square delta)))))
    (values n1 (round-up (* sample-ratio n1)))))

(defun binomial-test-paired-sse (pd pa &key (alpha 0.05)
                                         (1-beta 0.95) (tails :both))
  "Sample size estimate for the McNemar (discordant pairs) test. Pd is
the projected proportion of discordant pairs among all pairs, and Pa
is the projected proportion of type A pairs among discordant
pairs. alpha, 1-beta and tails are as binomal-test-two-sample-sse.
Returns the number of individuals necessary; that is twice the number
of matched pairs necessary."
  (let ((qa (- 1 pa))
        (z-alpha (z (- 1 (if (eql tails :both) (/ alpha 2) alpha))))
        (z-beta (z 1-beta)))
    (round-up (/ (square (+ z-alpha (* 2 z-beta (sqrt (* pa qa)))))
                 (* 2 (square (- pa 1/2)) pd)))))

(defun normal-pdf (x mu sigma)
  "The probability density function (PDF) for a normal distribution
with mean mu and variance sigma at point x."
  (* (/ (* (sqrt (* 2 pi)) sigma))
     (exp (* (- (/ (* 2 (square sigma)))) (square (- x mu))))))

(defun convert-to-standard-normal (x mu sigma)
  "Convert X from a Normal distribution with mean mu and variance
sigma to standard normal"
  (/ (- x mu) sigma))

(defun phi (x)
  "the CDF of standard normal distribution. Adopted from CLASP 1.4.3,
see copyright notice at http://eksl-www.cs.umass.edu/clasp.html"
  (* .5 (+ 1.0 (error-function (/ x (sqrt 2.0))))))

(defun z (percentile &key (epsilon 1d-15))
  "The inverse normal function, P(X<Zu) = u where X is distributed as
the standard normal. Uses binary search."
  (let ((target (coerce percentile 'double-float)))
    (do ((min -9d0 min)
         (max 9d0 max)
         (guess 0d0 (+ min (/ (- max min) 2d0))))
        ((< (- max min) epsilon) guess)
      (let ((result (coerce (phi guess) 'double-float)))
        (if (< result target)
            (setq min guess)
            (setq max guess))))))

(defun t-significance (t-statistic dof &key (tails :both))
  "Lookup table in Rosner; this is adopted from CLASP/Numeric
Recipes (CLASP 1.4.3), http://eksl-www.cs.umass.edu/clasp.html"
  (setf dof (float dof t-statistic))
  (let ((a (beta-incomplete (* 0.5 dof) 0.5 (/ dof (+ dof (square t-statistic))))))
    ;; A is 2*Integral from (abs t-statistic) to Infinity of t-distribution
    (ecase tails
      (:both a)
      (:positive (if (plusp t-statistic)
                     (* .5 a)
                     (- 1.0 (* .5 a))))
      (:negative (if (plusp t-statistic)
                     (- 1.0 (* .5 a))
                     (* .5 a))))))

(defun t-distribution (dof percentile)
  "Returns the point which is the indicated percentile in the T
distribution with dof degrees of freedom. Adopted from CLASP 1.4.3,
http://eksl-www.cs.umass.edu/clasp.html"
  (find-critical-value
   (lambda (x) (t-significance x dof :tails :positive))
   (- 1 percentile)))

(defun chi-square-cdf (x dof)
  "Computes the left hand tail area under the chi square distribution
under dof degrees of freedom up to X. Adopted from CLASP 1.4.3,
http://eksl-www.cs.umass.edu/clasp.html"
  (multiple-value-bind (cdf ignore)
      (gamma-incomplete (* 0.5 dof) (* 0.5 x))
    (declare (ignore ignore))
    cdf))

(defun chi-square (dof percentile)
  "Returns the point which is the indicated percentile in the Chi
Square distribution with dof degrees of freedom."
  (find-critical-value (lambda (x) (chi-square-cdf x dof))
                       (- 1 percentile)))

(defun normal-mean-ci (mean sd n alpha)
  "Confidence interval for the mean of a normal distribution
The 1-alpha percent confidence interval on the mean of a normal
distribution with parameters mean, sd & n."
  (let ((t-value (t-distribution (1- n) (- 1 (/ alpha 2)))))
    (values (- mean (* t-value (/ sd (sqrt n))))
            (+ mean (* t-value (/ sd (sqrt n)))))))

(defun normal-mean-ci-on-sequence (sequence alpha)
  "The 1-alpha confidence interval on the mean of a sequence of
numbers drawn from a Normal distribution."
  (multiple-value-call #'normal-mean-ci (mean-sd-n sequence) alpha))

(defun normal-variance-ci (variance n alpha)
  "The 1-alpha confidence interval on the variance of a sequence of
numbers drawn from a Normal distribution."
  (let ((chi-square-low (chi-square (1- n) (- 1 (/ alpha 2))))
        (chi-square-high (chi-square (1- n) (/ alpha 2)))
        (numerator (* (1- n) variance)))
    (values (/ numerator chi-square-low) (/ numerator chi-square-high))))

(defun normal-variance-ci-on-sequence (sequence alpha)
  (let ((variance (variance sequence))
        (n (length sequence)))
    (normal-variance-ci variance n alpha)))

(defun normal-sd-ci (sd n alpha)
  "As normal-variance-ci-on-sequence, but a confidence inverval for
the standard deviation."
  (multiple-value-bind (low high)
      (normal-variance-ci (square sd) n alpha)
    (values (sqrt low) (sqrt high))))

(defun normal-sd-ci-on-sequence (sequence alpha)
  (let ((sd (standard-deviation sequence))
        (n (length sequence)))
    (normal-sd-ci sd n alpha)))

(defun z-test (x-bar n &key (mu 0) (sigma 1) (tails :both))
  "The significance of a one sample Z test for the mean of a normal
distribution with known variance.
mu is the null hypothesis mean, x-bar is the observed mean, sigma is
the standard deviation and N is the number of observations.  If tails
is :both, the significance of a difference between x-bar and mu.  If
tails is :positive, the significance of x-bar is greater than mu, and
if tails is :negative, the significance of x-bar being less than mu.
Returns a p value."
  (let ((z (/ (- x-bar mu) (/ sigma (sqrt n)))))
    (ecase tails
      (:both (if (< z 0)
                 (* 2 (phi z))
                 (* 2 (- 1 (phi z)))))
      (:negative (phi z))
      (:positive (- 1 (phi z))))))

(defun z-test-on-sequence (sequence &key (mu 0) (sigma 1) (tails :both))
  (let ((x-bar (mean sequence))
        (n (length sequence)))
    (z-test x-bar n :mu mu :sigma sigma :tails tails)))

(defun f-significance
    (f-statistic numerator-dof denominator-dof &optional one-tailed-p)
  "Adopted from CLASP, but changed to handle F < 1 correctly in the
one-tailed case.  The `f-statistic' must be a positive number.  The
degrees of freedom arguments must be positive integers.  The
`one-tailed-p' argument is treated as a boolean.
This implementation follows Numerical Recipes in C, section 6.3 and
the `ftest' function in section 13.4."
  (setq f-statistic (float f-statistic))
  (let ((tail-area (beta-incomplete
                    (* 0.5d0 denominator-dof)
                    (* 0.5d0 numerator-dof)
                    (float (/ denominator-dof
                              (+ denominator-dof
                                 (* numerator-dof f-statistic))) 1d0))))
    (if one-tailed-p
        (if (< f-statistic 1) (- 1 tail-area) tail-area)
        (progn (setf tail-area (* 2.0 tail-area))
               (if (> tail-area 1.0)
                   (- 2.0 tail-area)
                   tail-area)))))

(defun f-test (variance1 n1 variance2 n2 &key (tails :both))
  "F test for the equality of two variances"
  (let ((significance (f-significance (/ variance1 variance2) (1- n1) (1- n2)
                                      (not (eql tails :both)))))
    (ecase tails
      (:both significance)
      (:positive (if (> variance1 variance2) significance (- 1 significance)))
      (:negative (if (< variance1 variance2) significance (- 1 significance))))))

(defun t-test-one-sample (x-bar sd n mu &key (tails :both))
  "The significance of a one sample T test for the mean of a normal
distribution with unknown variance. X-bar is the observed mean, sd is
the observed standard deviation, N is the number of observations and
mu is the test mean.
See also t-test-one-sample-on-sequence"
  (t-significance  (/ (- x-bar mu) (/ sd (sqrt n))) (1- n) :tails tails))

(defun t-test-one-sample-on-sequence (sequence mu &key (tails :both))
  "As t-test-one-sample, but calculates the observed values from a
sequence of numbers."
  (multiple-value-call #'t-test-one-sample
    (mean-sd-n sequence) mu :tails tails))

(defun t-test-paired (d-bar sd n &key (tails :both))
  "The significance of a paired t test for the means of two normal
distributions in a longitudinal study.  D-bar is the mean difference,
sd is the standard deviation of the differences, N is the number of
pairs."
  (t-significance (/ d-bar (/ sd (sqrt n))) (1- n) :tails tails))

(defun t-test-paired-on-sequences (before after &key (tails :both))
  "The significance of a paired t test for means of two normal
distributions in a longitudinal study.  Before is a sequence of before
values, after is the sequence of paired after values (which must be
the same length as the before sequence)."
  (multiple-value-call #'t-test-paired
    (mean-sd-n (map 'list #'- before after)) :tails tails))

(defun t-test-two-sample (x-bar1 sd1 n1 x-bar2 sd2 n2 &key
                                                        (variances-equal? :test)
                                                        (variance-significance-cutoff 0.05)
                                                        (tails :both))
  "The significance of the difference of two means (x-bar1 and x-bar2)
with standard deviations sd1 and sd2, and sample sizes n1 and n2
respectively.  The form of the two sample t test depends on whether
the sample variances are equal or not.  If the variable
variances-equal? is :test, then we use an F test and the
variance-significance-cutoff to determine if they are equal.  If the
variances are equal, then we use the two sample t test for equal
variances.  If they are not equal, we use the Satterthwaite method,
which has good type I error properties (at the loss of some power)."
  (let (t-statistic dof)
    (if (ecase variances-equal?
          (:test (> (f-test (square sd1) n1 (square sd2) n2 :tails tails)
                    variance-significance-cutoff))
          ((t :yes :equal) t)
          ((nil :no :unequal) nil))
        (let ((s (sqrt (/ (+ (* (1- n1) (square sd1))
                             (* (1- n2) (square sd2)))
                          (+ n1 n2 -2)))))
          (setq t-statistic  (/ (- x-bar1 x-bar2)
                                (* s (sqrt (+ (/ n1) (/ n2)))))
                dof (+ n1 n2 -2)))
        (let* ((variance-ratio1 (/ (square sd1) n1))
               (variance-ratio2 (/ (square sd2) n2)))
          (setq t-statistic (/ (- x-bar1 x-bar2)
                               (sqrt (+ variance-ratio1 variance-ratio2)))
                dof (round (/ (square (+ variance-ratio1 variance-ratio2))
                              (+ (/ (square variance-ratio1) (1- n1))
                                 (/ (square variance-ratio2) (1- n2))))))))
    (t-significance t-statistic dof :tails tails)))

(defun t-test-two-sample-on-sequences (sequence1 sequence2 &key
                                                             (variance-significance-cutoff 0.05)
                                                             (tails :both))
  "Same as t-test-two-sample, but providing the sequences rather than
the summaries."
  (multiple-value-call #'t-test-two-sample
    (mean-sd-n sequence1) (mean-sd-n sequence2) :tails tails
    :variance-significance-cutoff variance-significance-cutoff))

(defun t-test-one-sample-sse (mu mu-null variance &key
                                                    (alpha 0.05) (1-beta .95) (tails :both))
  "Returns the number of subjects needed to test whether the mean of a
normally distributed sample mu is different from a null hypothesis
mean mu-null and variance variance, with alpha, 1-beta and tails as
specified."
  (let ((z-beta (z 1-beta))
        (z-alpha (z (- 1 (if (eql tails :both) (/ alpha 2) alpha)))))
    (round-up (/ (* variance (square (+ z-beta z-alpha)))
                 (square (- mu-null mu))))))

(defun t-test-two-sample-sse (mu1 variance1 mu2 variance2 &key
                                                            (sample-ratio 1) (alpha 0.05)
                                                            (1-beta .95) (tails :both))
  "Returns the number of subjects needed to test whether the mean mu1 of
a normally distributed sample (with variance variance1) is different
from a second sample with mean mu2 and variance variance2, with alpha,
1-beta and tails as specified.  It is also possible to set a sample
size ratio of sample 1 to sample 2."
  (let* ((delta2 (square (- mu1 mu2)))
         (z-term (square (+ (z 1-beta)
                            (z (- 1 (if (eql tails :both)
                                        (/ alpha 2)
                                        alpha))))))
         (n1 (round-up (/ (* (+ variance1 (/ variance2 sample-ratio)) z-term)
                          delta2))))
    (values n1 (round-up (* sample-ratio n1)))))

(defun t-test-paired-sse (difference-mu difference-variance
                          &key (alpha 0.05) (1-beta 0.95)
                            (tails :both))
  "Returns the number of subjects needed to test whether the differences
with mean difference-mu and variance difference-variance, with alpha,
1-beta and tails as specified."
  (round-up (/ (* 2 difference-variance
                  (square (+ (z 1-beta)
                             (z (- 1 (if (eql tails :both)
                                         (/ alpha 2)
                                         alpha))))))
               (square difference-mu))))

(defun chi-square-test-one-sample (variance n sigma-squared &key (tails :both))
  "The significance of a one sample Chi square test for the variance of
a normal distribution.  Variance is the observed variance, N is the
number of observations, and sigma-squared is the test variance."
  (let ((cdf (chi-square-cdf (/ (* (1- n) variance) sigma-squared) (1- n))))
    (ecase tails
      (:negative cdf)
      (:positive (- 1 cdf))
      (:both (if (<= variance sigma-squared)
                 (* 2 cdf)
                 (* 2 (- 1 cdf)))))))

(defun chi-square-test-rxc (contingency-table)
  "Takes contingency-table, an RxC array, and returns the significance
of the relationship between the row variable and the column variable.
Any difference in proportion will cause this test to be significant --
consider using the test for trend instead if you are looking for a
consistent change."
  (let* ((rows (array-dimension contingency-table 0))
         (columns (array-dimension contingency-table 1))
         (row-marginals (make-array rows :initial-element 0.0))
         (column-marginals (make-array columns :initial-element 0.0))
         (total 0.0)
         (expected-lt-5 0)
         (expected-lt-1 0)
         (expected-values (make-array (list rows columns)
                                      :element-type 'single-float))
         (x2 0.0))
    (dotimes (i rows)
      (dotimes (j columns)
        (let ((cell (aref contingency-table i j)))
          (incf (svref row-marginals i) cell)
          (incf (svref column-marginals j) cell)
          (incf total cell))))
    (dotimes (i rows)
      (dotimes (j columns)
        (let ((expected (/ (* (aref row-marginals i) (aref column-marginals j))
                           total)))
          (when (< expected 1) (incf expected-lt-1))
          (when (< expected 5) (incf expected-lt-5))
          (setf (aref expected-values i j) expected))))
    (when (plusp expected-lt-1)
      (error "This test cannot be used when an expected value is less than one"))
    (when (> expected-lt-5 (/ (* rows columns) 5))
      (error "This test cannot be used when more than 1/5 of the expected values are less than 5."))
    (dotimes (i rows)
      (dotimes (j columns)
        (incf x2 (/ (square (- (aref contingency-table i j)
                               (aref expected-values i j)))
                    (aref expected-values i j)))))
    (- 1 (chi-square-cdf x2 (* (1- rows) (1- columns))))))

(defun chi-square-test-for-trend (row1-counts row2-counts &optional scores)
  "This test works on a 2xk table and assesses if there is an increasing
or decreasing trend.  Arguments are equal sized lists counts.
Optionally, provide a list of scores, which represent some numeric
attribute of the group.  If not provided, scores are assumed to be 1
to k."
  (unless scores (setq scores (dotimes (i (length row1-counts) (nreverse scores))
                                (push (1+ i) scores))))
  (let* ((ns (map 'list #'+ row1-counts row2-counts))
         (p-hats (map 'list #'/ row1-counts ns))
         (n (reduce #'+ ns))
         (p-bar (/ (reduce #'+ row1-counts) n))
         (q-bar (- 1 p-bar))
         (s-bar (mean scores))
         (a (reduce #'+ (mapcar (lambda (p-hat ni s)
                                  (* ni (- p-hat p-bar) (- s s-bar)))
                                p-hats ns scores)))
         (b (* p-bar q-bar (- (reduce #'+ (mapcar (lambda (ni s) (* ni (square s)))
                                                  ns scores))
                              (/ (square (reduce #'+ (mapcar (lambda (ni s) (* ni s))
                                                             ns scores)))
                                 n))))
         (x2 (/ (square a) b))
         (significance (- 1 (chi-square-cdf (float x2) 1))))
    (when (< (* p-bar q-bar n) 5)
      (error "This test is only applicable when N * p-bar * q-bar >= 5"))
    (format t "~%The trend is ~a, p = ~f"
            (if (< a 0) "decreasing" "increasing")
            significance)
    significance))

(defun sign-test (plus-count minus-count &key (exact? nil) (tails :both))
  "Really just a special case of the binomial one sample test with p =
1/2. The normal theory version has a correction factor to make it a
better approximation."
  (let* ((n (+ plus-count minus-count))
         (p-hat (/ plus-count n)))
    (if (or (< n 20) exact?)
        (binomial-test-one-sample p-hat n 0.5 :tails tails :exact? t)
        (let ((area (- 1 (phi (/ (1- (abs (- plus-count minus-count)))
                                 (sqrt n))))))
          (if (eql tails :both)
              (* 2 area)
              area)))))

(defun sign-test-on-sequences (sequence1 sequence2 &key (exact? nil) (tails :both))
  "Same as sign-test, but takes two sequences and tests whether the
entries in one are different (greater or less) than the other."
  (let* ((differences (map 'list #'- sequence1 sequence2))
         (plus-count (count #'plusp differences))
         (minus-count (count #'minusp differences)))
    (sign-test plus-count minus-count :exact? exact? :tails tails)))

(defun wilcoxon-signed-rank-test (differences &optional (tails :both))
  "A test on the ranking of positive and negative differences (are the
positive differences significantly larger/smaller than the negative
ones). Assumes a continuous and symmetric distribution of differences,
although not a normal one.  This is the normal theory approximation,
which is only valid when N > 15.
This test is completely equivalent to the Mann-Whitney test."
  (let* ((nonzero-differences (remove 0 differences :test #'=))
         (sorted-list (sort (mapcar (lambda (dif)
                                      (list (abs dif) (sign dif)))
                                    nonzero-differences)
                            #'<
                            :key #'first))
         (distinct-values (delete-duplicates (mapcar #'first sorted-list)))
         (ties nil))

    (when (< (length nonzero-differences) 16)
      (error "This Wilcoxon Signed-Rank Test (normal approximation method) requires nonzero N > 15"))

    (unless (member tails '(:positive :negative :both))
      (error "tails must be one of :positive, :negative or :both, not ~a" tails))

                                        ; add avg-rank to the sorted values

    (dolist (value distinct-values)
      (let ((first (position value sorted-list :key #'first))
            (last (position value sorted-list :key #'first :from-end t)))
        (if (= first last)
            (nconc (find value sorted-list :key #'first) (list (1+ first)))
            (let ((number-tied (1+ (- last first)))
                  (avg-rank (1+ (/ (+ first last) 2)))) ; +1 since 0 based
              (push number-tied ties)
              (dotimes (i number-tied)
                (nconc (nth (+ first i) sorted-list) (list avg-rank)))))))
    (setq ties (nreverse ties))
    (let* ((direction (if (eq tails :negative) -1 1))
           (r1 (reduce #'+ (mapcar (lambda (entry)
                                     (if (= (second entry) direction)
                                         (third entry)
                                         0))
                                   sorted-list)))
           (n (length nonzero-differences))
           (expected-r1 (/ (* n (1+ n)) 4))
           (ties-factor (if ties
                            (/ (reduce #'+ (mapcar (lambda (ti)
                                                     (- (* ti ti ti) ti))
                                                   ties))
                               48)
                            0))
           (var-r1 (- (/ (* n (1+ n) (1+ (* 2 n))) 24) ties-factor))
           (T-score (/ (- (abs (- r1 expected-r1)) 1/2) (sqrt var-r1))))
      (* (if (eq tails :both) 2 1) (- 1 (phi T-score))))))

(defun wilcoxon-signed-rank-test-on-sequences (sequence1 sequence2
                                               &optional (tails :both))
  (wilcoxon-signed-rank-test (map 'list #'- sequence1 sequence2) tails))

(defun correlation-sse (rho &key (alpha 0.05) (1-beta 0.95))
  "Returns the size of a sample necessary to find a correlation of
expected value rho with significance alpha and power 1-beta."
  (round-up (+ 3 (/ (square (+ (z (- 1 alpha)) (z 1-beta)))
                    (square (fisher-z-transform rho))))))

(defun linear-regression (points)
  "Computes the regression equation for a least squares fit of a line to
a sequence of points (each a list of two numbers, e.g. '((1.0
0.1) (2.0 0.2))) and report the intercept, slope, correlation
coefficient r, R^2, and the significance of the difference of the
slope from 0."
  (let  ((xs (map 'list #'first points))
         (ys (map 'list #'second points)))
    (let* ((x-bar (mean xs))
           (y-bar (mean ys))
           (n (length points))
           (Lxx (reduce #'+ (mapcar (lambda (xi) (square (- xi x-bar))) xs)))
           (Lyy (reduce #'+ (mapcar (lambda (yi) (square (- yi y-bar))) ys)))
           (Lxy (reduce #'+ (mapcar (lambda (xi yi) (* (- xi x-bar) (- yi y-bar)))
                                    xs ys)))
           (b (/ Lxy Lxx))
           (a (- y-bar (* b x-bar)))
           (reg-ss (* b Lxy))
           (res-ms (/ (- Lyy reg-ss) (- n 2)))
           (r (/ Lxy (sqrt (* Lxx Lyy))))
           (r2 (/ reg-ss Lyy))
           (t-test (/ b (sqrt (/ res-ms Lxx))))
           (t-significance (t-significance t-test (- n 2) :tails :both)))
      (format t "~%Intercept = ~f, slope = ~f, r = ~f, R^2 = ~f, p = ~f"
              a b r r2 t-significance)
      (values a b r r2 t-significance))))

(defun correlation-coefficient (points)
  "just r from linear-regression.  Also called Pearson Correlation"
  (let ((xs (map 'list #'first points))
        (ys (map 'list #'second points)))
    (let ((x-bar (mean xs))
          (y-bar (mean ys)))
      (/ (reduce #'+ (mapcar (lambda (xi yi) (* (- xi x-bar) (- yi y-bar)))
                             xs ys))
         (sqrt (* (reduce #'+ (mapcar (lambda (xi) (square (- xi x-bar)))
                                      xs))
                  (reduce #'+ (mapcar (lambda (yi) (square (- yi y-bar)))
                                      ys))))))))

(defun correlation-test-two-sample (r1 n1 r2 n2 &key (tails :both))
  "Test if two correlation coefficients are different. Users Fisher's Z
test."
  (let* ((z1 (fisher-z-transform r1))
         (z2 (fisher-z-transform r2))
         (lambda (/ (- z1 z2) (sqrt (+ (/ (- n1 3)) (/ (- n2 3)))))))
    (ecase tails
      (:both (* 2 (if (<= lambda 0) (phi lambda) (- 1 (phi lambda)))))
      (:positive (- 1 (phi lambda)))
      (:negative (phi lambda)))))

(defun correlation-test-two-sample-on-sequences (points1 points2 &key (tails :both))
  (let ((r1 (correlation-coefficient points1))
        (n1 (length points1))
        (r2 (correlation-coefficient points2))
        (n2 (length points2)))
    (correlation-test-two-sample r1 n1 r2 n2 :tails tails)))

(defun spearman-rank-correlation (points)
  "Spearman rank correlation computes the relationship between a pair of
variables when one or both are either ordinal or have a distribution
that is far from normal.  It takes a list of points (same format as
linear-regression) and returns the spearman rank correlation
coefficient and its significance."
  (let ((xis (mapcar #'first points))
        (yis (mapcar #'second points)))
    (let* ((n (length points))
           (sorted-xis (sort (copy-seq xis) #'<))
           (sorted-yis (sort (copy-seq yis) #'<))
           (average-x-ranks (mapcar (lambda (x) (average-rank x sorted-xis)) xis))
           (average-y-ranks (mapcar (lambda (y) (average-rank y sorted-yis)) yis))
           (mean-x-rank (mean average-x-ranks))
           (mean-y-rank (mean average-y-ranks))
           (Lxx (reduce #'+ (mapcar (lambda (xi-rank) (square (- xi-rank mean-x-rank)))
                                    average-x-ranks)))
           (Lyy (reduce #'+ (mapcar (lambda (yi-rank) (square (- yi-rank mean-y-rank)))
                                    average-y-ranks)))
           (Lxy (reduce #'+ (mapcar (lambda (xi-rank yi-rank)
                                      (* (- xi-rank mean-x-rank)
                                         (- yi-rank mean-y-rank)))
                                    average-x-ranks average-y-ranks)))
           (rs (/ Lxy (sqrt (* Lxx Lyy))))
           (ts (/ (* rs (sqrt (- n 2))) (sqrt (- 1 (square rs)))))
           (p (t-significance ts (- n 2) :tails :both)))
      (format t "~%Spearman correlation coefficient ~f, p = ~f" rs p)
      (values rs p))))

(defun random-pick (sequence)
  "Random selection from sequence"
  (when sequence (elt sequence (random (length sequence)))))

(defun random-sample (n sequence)
  "Return a random sample of size N from sequence, without replacement.
If N is equal to or greater than the length of the sequence, return
the entire sequence."
  (cond ((null sequence) nil)
        ((<= n 0) nil)
        ((< (length sequence) n) sequence)
        (t (let ((one (random-pick sequence)))
             (cons one (random-sample (1- n) (remove one sequence :count 1)))))))

(defun random-normal (&key (mean 0) (sd 1))
  "returns a random number with mean and standard-distribution as
specified."
  (let ((random-standard (z (random 1d0))))
    (+ mean (* sd random-standard))))

(defun false-discovery-correction (p-values &key (rate 0.05))
  "A multiple testing correction that is less conservative than Bonferroni.
Takes a list of p-values and a false discovery rate, and returns the
number of p-values that are likely to be good enough to reject the
null at that rate.  Returns a second value which is the p-value
cutoff. See
  Benjamini Y and Hochberg Y. \"Controlling the false discovery rate:
  a practical and powerful approach to multiple testing.\" J R Stat
  Soc Ser B 57: 289 300, 1995."
  (let ((number-of-tests (length p-values))
        (sorted-p-values (sort p-values #'>)))
    (do ((p-value (pop sorted-p-values) (pop sorted-p-values))
         (tests-to-go number-of-tests (1- tests-to-go)))
        ((or (null p-value)
             (<= p-value (* rate (/ tests-to-go number-of-tests))))
         (values tests-to-go (* rate (/ tests-to-go number-of-tests)))))))

(defun t1-value (values target)
  (/ (- (mean values) target)
     (/ (standard-deviation values)
	(sqrt (length values)))))

(defvar *t-cdf-critical-points-table-for-.05*
  '((1 . 6.31)
    (2 . 2.92)
    (3 . 2.35)
    (4 . 2.13)
    (5 . 2.02)
    (6 . 1.94)
    (7 . 1.89)
    (8 . 1.86)
    (9 . 1.83)
    (10 . 1.81)
    (15 . 1.75)
    (20 . 1.72)
    (25 . 1.71)
    (30 . 1.70)
    (40 . 1.68)
    (60 . 1.67)
    (120 . 1.66)))

(defun t-p-value (x df &optional (warn? t))
  (if (> df 120)
      (progn
	(if warn?
	    (format t "Warning df (~a) is off the scale.  Using df=120~%" df))
	(setq df 120)))
  (dolist (tcp *t-cdf-critical-points-table-for-.05*)
    (if (or (= df (car tcp))
	    (< df (car tcp)))
	(return (if (> x (cdr tcp)) '*/p<.05! nil)))))

(defun t1-test (values target &optional (warn? t))
  "One way t-test to see if a group differs from a numerical mean
target value.  From Misanin & Hinderliter p. 248."
  (let ((t1-value (t1-value values target)))
    (values t1-value
	    (t-p-value (abs t1-value) (1- (length values)) warn?))))

(defun n-random (n l &aux r)
  "Select n random sublists from a list, without replacement.  This
copies the list and then destroys the copy.  N better be less than or
equal to (length l)."
  (setq l (copy-list l))
  (dotimes (k n)
    (if (null (cdr l)) ; this has to be the last one
        (progn (push (car l) r)
	       (return nil))
        ;; If we're taking the first, it's easy, otherwise
        ;; the first item get shoved into the middle.
        (let ((take (random (length l))))
          (if (zerop take)
              (push (car l) r)
              (let ((nc (nthcdr take l)))
                (push (car nc) r)
                (rplaca nc (car l))))
          (pop l))))
  r)

(defun s2 (l n)
  (/ (- (sum (mapcar (lambda (a) (* a a)) l))
	(/ (let ((r (sum l))) (* r r)) n))
     (1- n)))

(defun t2-value (l1 l2)
  (let* ((n1 (float (length l1)))
	 (n2 (float (length l2)))
	 (s21 (s2 l1 n1))
	 (s22 (s2 l2 n2))
	 (m1 (mean l1))
	 (m2 (mean l2))
 	 (s2p (/ (+ (* s21 (1- n1))
		    (* s22 (1- n2)))
		 (+ (1- n1) (1- n2))))
	 )
    (/ (- m1 m2)
       (sqrt (+ (/ s2p n1) (/ s2p n2))))))

(defun t2-test (l1 l2)
  "T2-test calculates an UNPAIRED t-test.
From Misanin & Hinderliter p. 268.  The t-cdf part is inherent in
xlispstat, and I'm not entirely sure that it's really the right
computation since it doens't agree entirely with Table 5 of M&H, but
it's close, so I assume that M&H have round-off error."
  (if (or (equal l1 l2) ; protect against 0 result from sdiff!
	  (null (cdr l1)) ; and other stupidity!
	  (null (cdr l2)))
      (values "n/a" "n/a")
      (let ((tval (t2-value l1 l2)))
        (values tval (t-p-value (abs tval) (- (+ (length l1) (length l2)) 2))))))

(defun anova2 (a1b1 a1b2 a2b1 a2b2)
  "Two-Way Anova.  (From Misanin & Hinderliter, 1991, p. 367-) This is
specialized for four groups of equal n, called by their plot location
names: left1 left2 right1 right2."
  (let* ((n (length a1b1)) ; All are the same, so any will do here.
         (npq (* 4 n)) ; This is specialized for a 2x2 design; always 4 levels.
	 (a1 (append a1b1 a1b2))
	 (suma1 (sum a1))
	 (a2 (append a2b1 a2b2))
	 (suma2 (sum a2))
	 (b1 (append a1b1 a2b1))
	 (sumb1 (sum b1))
	 (b2 (append a1b2 a2b2))
	 (sumb2 (sum b2))
	 (allscores (append a1 a2))
	 (sym1 (float (/ (sqr (sum allscores)) npq)))
	 (sym2 (float (sum (sqr allscores))))
	 (np (* n 2))
	 (sym3 (float (/ (+ (sqr suma1) (sqr suma2)) np)))
                                        ;(nq np) ; Apparently never used???
	 (sym4 (float (/ (+ (sqr sumb1) (sqr sumb2)) np)))
	 (sym5 (float (/ (+ (sqr (sum a1b1)) (sqr (sum a2b1))
			    (sqr (sum a1b2)) (sqr (sum a2b2)))
			 n)
		      ))
	 (ssbg (- sym5 sym1))
	 (ssa (- sym3 sym1))
	 (ssb (- sym4 sym1))
	 (ssab (+ sym1 (- (- sym5 sym4) sym3)))
	 (sswg (- sym2 sym5))
	 (sstot (- sym2 sym1))
	 (df1 3)
	 (df2 1)
	 (df3 1)
	 (df4 1)
	 (df5 (* 4 (1- n)))
	 (dftot (1- npq))
	 (msbg (float (/ ssbg df1)))
	 (msa (float (/ ssa df2)))
	 (msb (float (/ ssb df3)))
	 (msab (float (/ ssab df4)))
	 (mswg (float (/ sswg df5))))
    (list :ssbg ssbg :dfbg df1 :msbg msbg :fbg (/ msbg mswg)
	  :ssa ssa :dfa df2 :msa msa :fa (/ msa mswg)
	  :ssb ssb :dfb df3 :msb msb :fb (/ msb mswg)
	  :ssab ssab :dfab df4 :msab msab :fab (/ msab mswg)
	  :sswg sswg :dfwg df5 :mswg mswg
	  :sstot sstot :dftot dftot
	  :a1b1 a1b1 :a1b2 a1b2 :a2b1 a2b1 :a2b2 a2b2)))

(defun testanova2 ()
  (let ((a1b1 '(1 2 3 4 5))
	(a1b2 '(3 4 5 6 7))
	(a2b1 '(5 6 7 8 9))
	(a2b2 '(4 5 6 7 8)))
    (anova2 a1b1 a1b2 a2b1 a2b2)))

(defun anova2r (g1 g2)
  "Two way ANOVA with repeated measures on one dimension.  From
Ferguson & Takane, 1989, p. 359.  Data is organized differently for
this test.  Each group (g1 g2) contains list of all subjects' repeated
measures, and same for B.  So, A: ((t1s1g1 t2s1g1 ...) (t1s2g2 t2s2g2
...) ...)  Have to have the same number of test repeats for each
subject, and this assumes the same number of subject in each group."
  (let* ((c (length (car g1)))
	 (r 2) ; only two groups in this special case
	 (tsr1 (mapcar #'sum g1))
	 (t1 (sum tsr1))
	 (t1c (let (temp)
		(dotimes (n c temp)
                  (push (sum (mapcar (lambda (s) (nth n s)) g1)) temp))))
	 (tsr2 (mapcar #'sum g2))
	 (t2 (sum tsr2))
	 (t2c (let (temp)
		(dotimes (n c temp)
                  (push (sum (mapcar (lambda (s) (nth n s)) g2)) temp))))
	 (tc (mapcar #'+ t1c t2c))
	 (total (+ t1 t2))
	 (n (length g1))
	 (q1 (* (/ 1.0 c) (sum (append (sqr tsr1) (sqr tsr2)))))
	 (q2 (* (/ 1.0 (* c n)) (+ (sqr t1) (sqr t2))))
	 (q3 (* (/ 1.0 (* 2 n)) (sum (sqr tc))))
	 (q4 (* (/ 1.0 n) (sum (append (sqr t1c) (sqr t2c)))))
	 (q5 (sum (append (mapcar #'sum (mapcar #'sqr g1))
			  (mapcar #'sum (mapcar #'sqr g2)))))
	 (q6 (/ (sqr total) (* n 2.0 c)))
	 (ssbs (- q1 q6))
	 (ssr (- q2 q6))
	 (sssr (- q1 q2))
	 (ssws (- q5 q1))
	 (ssc (- q3 q6))
	 (ssrc (+ q6 (- (- q4 q2) q3)))
	 (sscsr (+ q2 (- (- q5 q1) q4)))
	 (sstotal (- q5 q6))
	 (dfr (1- r))
	 (dfc (1- c))
	 (dfrc (* (1- r) (1- c)))
	 (dfsr (* r (1- n)))
	 (dfcsr (* r (1- n) (1- c)))
	 (msr (/ ssr dfr))
	 (mssr (/ sssr dfsr))
	 (msc (/ ssc dfc))
	 (msrc (/ ssrc dfrc))
	 (mscsr (/ sscsr dfcsr))
	 (dftotal (+ dfr dfc dfrc dfsr dfcsr))
	 (fr (/ msr mssr))
	 (fc (/ msc mscsr))
	 (frc (/ ssrc mscsr)))
    (if (not (= (length g1) (length g2)))
	(format t "Warning, ANOVA2R design has unbalanced cells!~%"))
    (list :ssbs ssbs :ssr ssr :sssr sssr :ssws ssws :ssc ssc
	  :ssrc ssrc :sscsr sscsr :sstotal sstotal
	  :dfr dfr :dfsr dfsr :dfc dfc :dfrc dfrc :dfcsr dfcsr :dftotal dftotal
	  :msr msr :mssr mssr :msc msc :msrc msrc :mscsr mscsr
	  :fr fr :fc fc :frc frc)))

(defun anova1 (d) ; Note that dots are replaced by dashes, so: Y.. = Y-- here
  "One way simple ANOVA, from Neter, et al. p677+. Data is give as a
list of lists, each one representing a treatment, and each containing
the observations."
  (let* ((meanYi- (mapcar #'mean d))
	 (serrYi- (mapcar #'standard-error d))
	 (r (length d))
	 (Yi- (mapcar #'sum d))
	 (ni (mapcar #'length d))
	 (Y-- (sum Yi-))
	 (meanY-- (mean meanYi-))
	 (ntotal (sum ni))
	 (SSTO (sum (mapcar (lambda (treatment)
                              (sum (mapcar (lambda (Oij)
                                             (expt (- Oij meanY--) 2)) treatment))) d)))
	 (SSE (sum (mapcar (lambda (treatment meanYi-)
                             (sum (mapcar (lambda (Oij)
                                            (expt (- Oij meanYi-) 2)) treatment))) d meanYi-)))
	 (SSTR (sum (mapcar (lambda (meanYi- ni) (* ni (expt (- meanYi- meanY--) 2))) meanYi- ni)))
	 (SSTRdf (- r 1))
	 (SSEdf (- ntotal r))
	 (SSTOdf (- ntotal 1))
	 (MSTR (/ SSTR SSTRdf))
	 (MSE (/ SSE SSedf))
	 (F* (/ MSTR MSE)))
    (list :SUMMARY (format nil "F(.95,~a,~a) = ~a" SSTRdf SSEdf F*)
          :r r
          :meanYi- meanYi-
          :serrYi- serrYi-
          :Yi- Yi-
          :ni ni
          :Y-- Y--
          :meanY-- meanY--
          :ntotal ntotal
          :SSTO SSTO
          :SSE SSE
          :SSTR SSTR
          :SSTRdf SSTRdf
          :SSEdf SSEdf
          :SSTOdf SSTOdf
          :MSTR MSTR
          :MSE MSE
          :F* F*)))

(defun regress (x y)
  "Simple linear regression."
  (let* ((n (float (length x)))
	 (sumx (sum x))
	 (sumy (sum y))
	 (sumxy (sum (mapcar #'* x y)))
	 (sumx2 (sum (mapcar #'* x x)))
	 (m (/ (- (* n sumxy) (* sumx sumy))
	       (- (* n sumx2) (expt sumx 2))))
	 (b (+ (/ (* (- m) sumx) n)
	       (/ sumy n)))
	 (sumy2 (sum (mapcar #'* y y)))
	 (resids (mapcar (lambda (x y) (- y (+ (* x m) b))) x y))
	 (r (/ (- (* n sumxy) (* sumx sumy))
	       (sqrt (*
		      (- (* n sumx2) (expt (sum x) 2))
		      (- (* n sumy2) (expt (sum y) 2))
		      ))))
	 (r2 (expt r 2)))
    (list :m m :b b :resids resids :r r :r2 r2)))

(defvar *critical-values-of-r-two-tailed-column-interpretaion* '(0.2 0.1 0.05 0.02 0.01 0.001))

(defvar *critical-values-of-r* '(
                                 ;; n ... 2-tailed testing / (1-tailed testing)
                                 ;;   0.2 (0.1) 0.1 (0.05) 0.05 (0.025) 0.02 (0.01) 0.01 (0.005) 0.001 (0.0005)
                                 (5 0.687 0.805 0.878 0.934 0.959 0.991)
                                 (6 0.608 0.729 0.811 0.882 0.917 0.974)
                                 (7 0.551 0.669 0.754 0.833 0.875 0.951)
                                 (8 0.507 0.621 0.707 0.789 0.834 0.925)
                                 (9 0.472 0.582 0.666 0.750 0.798 0.898)
                                 (10 0.443 0.549 0.632 0.715 0.765 0.872)
                                 (11 0.419 0.521 0.602 0.685 0.735 0.847)
                                 (12 0.398 0.497 0.576 0.658 0.708 0.823)
                                 (13 0.380 0.476 0.553 0.634 0.684 0.801)
                                 (14 0.365 0.458 0.532 0.612 0.661 0.780)
                                 (15 0.351 0.441 0.514 0.592 0.641 0.760)
                                 (16 0.338 0.426 0.497 0.574 0.623 0.742)
                                 (17 0.327 0.412 0.482 0.558 0.606 0.725)
                                 (18 0.317 0.400 0.468 0.543 0.590 0.708)
                                 (19 0.308 0.389 0.456 0.529 0.575 0.693)
                                 (20 0.299 0.378 0.444 0.516 0.561 0.679)
                                 (21 0.291 0.369 0.433 0.503 0.549 0.665)
                                 (22 0.284 0.360 0.423 0.492 0.537 0.652)
                                 (23 0.277 0.352 0.413 0.482 0.526 0.640)
                                 (24 0.271 0.344 0.404 0.472 0.515 0.629)
                                 (25 0.265 0.337 0.396 0.462 0.505 0.618)
                                 (26 0.260 0.330 0.388 0.453 0.496 0.607)
                                 (27 0.255 0.323 0.381 0.445 0.487 0.597)
                                 (28 0.250 0.317 0.374 0.437 0.479 0.588)
                                 (29 0.245 0.311 0.367 0.430 0.471 0.579)
                                 (30 0.241 0.306 0.361 0.423 0.463 0.570)
                                 (40 0.207 0.264 0.312 0.367 0.403 0.501)
                                 (50 0.184 0.235 0.279 0.328 0.361 0.451)
                                 (60 0.168 0.214 0.254 0.300 0.330 0.414)
                                 (80 0.145 0.185 0.220 0.260 0.286 0.361)
                                 (100 0.129 0.165 0.197 0.232 0.256 0.324)
                                 (120 0.118 0.151 0.179 0.212 0.234 0.297)
                                 (140 0.109 0.140 0.166 0.196 0.217 0.275)
                                 (160 0.102 0.130 0.155 0.184 0.203 0.258)
                                 (180 0.096 0.123 0.146 0.173 0.192 0.243)
                                 (200 0.091 0.117 0.139 0.164 0.182 0.231)
                                 (300 0.074 0.095 0.113 0.134 0.149 0.189)
                                 (400 0.064 0.082 0.098 0.116 0.129 0.164)
                                 (500 0.057 0.074 0.088 0.104 0.115 0.147)))

(defun 2-tailed-correlation-significance (n r)
  "We use the first line for anything less than 5, and the last line
for anything over 500. Otherwise, find the nearest value (maybe we
should interpolate ... too much bother!)"
  (let ((target-row (first *critical-values-of-r*)))
    (when (> n 5)
      (loop for row in *critical-values-of-r*
            as (row-n . nil) = row
            with last-row-n = 5
            with last-row = target-row
            do
               (cond ((= row-n n)
                      (setq target-row row)
                      (return t))
                     ((> row-n n)
                      (cond ((< (abs (- n row-n))
                                (abs (- n last-row-n)))
                             (setq target-row row))
                            (t (setq target-row last-row)))
                      (return t)))
               (setq last-row row)
               (setq last-row-n row-n)
            finally (progn (setq target-row row)
                           (return t))))
    (pop target-row) ; removes the N header
    (cond ((< r (car target-row)) ">0.2")
	  (t (loop for crit in (cdr target-row)
                   as p in (cdr *critical-values-of-r-two-tailed-column-interpretaion*)
                   with last-p = (car *critical-values-of-r-two-tailed-column-interpretaion*)
                   when (< r crit)
                     do (return (format nil "<~a" last-p))
                   else do (setq last-p p)
                   finally (return (format nil "<~a" p)))))))

(defun correlate (x y)
  "Correlation of two sequences, as in Ferguson & Takane, 1989,
p. 125.  Assumes NO MISSING VALUES!"
  (if (not (= (length x) (length y)))
      (break "Can only correlate equal-sized sets."))
  (let* ((mx (mean x))
         (my (mean y))
	 (n (length x))
         (devx (mapcar (lambda (v) (- v mx)) x))
         (devy (mapcar (lambda (v) (- v my)) y))
	 (sumdevxy (sum (mapcar #'* devx devy)))
	 (sumsqdevx (sum (sqr devx)))
	 (sumsqdevy (sum (sqr devy)))
	 (r (/ sumdevxy (sqrt (* sumsqdevx sumsqdevy)))))
    (list :r r :r2 (sqr r) :n n :p (2-tailed-correlation-significance n (abs r)))))

(defun normalize (v)
  "Normalize a vector by dividing it through by subtracting its min
and then dividing through by its range (max-min).  If the numbers are
all the same, this would screw up, so we check that first and just
return a long list of 0.5 if so!"
  (let* ((mx (reduce #'max v))
	 (mn (reduce #'min v))
	 (range (float (- mx mn))))
    (mapcar (lambda (i) (if (zerop range) 0.5
                       (/ (- i mn) range))) v)))
