(declaim (optimize (speed 3) (debug 1) (safety 0) (compilation-speed 0)))

(in-package :mu)

;; XXX this is from https://github.com/telephil/cl-ascii-table/

(defclass xtbl ()
  ((header :initarg :header :reader header :initform nil)
   (cols :initarg :cols :accessor cols :initform nil)
   (cols-count :initarg :cols-count :accessor cols-count :initform 0)
   (cols-widths :initarg :cols-widths :accessor cols-widths :initform nil)
   (rows :accessor rows :initform nil)
   (cols-formatters :initarg :cols-formatters :accessor cols-formatters :initform nil)
   (has-tail-p :initarg :tailp :accessor has-tail-p :initform nil)))

(defgeneric xtbl/row (xtbl columns))
(defgeneric xtbl/separator (xtbl))
(defgeneric xtbl/display (xtbl &optional out))

(defvar *default-value-formatter*
  (lambda (value)
    (cond ((floatp value) (cond ((zerop (abs value)) (format nil "0"))
                                ((= 1 (abs value)) (format nil "~A" value))
                                ((> (abs value) 100) (format nil "~,1F" value))
                                ((> (abs value) 10) (format nil "~,2F" value))
                                ((> (abs value) 1) (format nil "~,4F" value))
                                ((> (abs value) 0.01) (format nil "~,4F" value))
                                ((> (abs value) 0.001) (format nil "~,4F" value))
                                (T (format nil "~,4F" value))))
          (T (format nil "~A" value)))))

(defun compute-col-width (col &optional (len 0) (formatter *default-value-formatter*))
  (let* ((formatted-value (funcall formatter col)))
    (max len (+ 2 (length formatted-value)))))

(defun xtbl (columns &key (header nil) tailp formatters)
  (when (> (length columns) (length formatters))
    (setf formatters
          (append formatters
                  (make-list (- (length columns)
                                (length formatters))
                             :initial-element *default-value-formatter*))))

  (make-instance 'xtbl
                 :header header
                 :cols columns
                 :cols-count (length columns)
                 :cols-widths (mapcar #'compute-col-width columns)
                 :cols-formatters formatters
                 :tailp tailp))

(defmethod $ ((xtbl xtbl) location &rest others-and-default)
  (declare (ignore others-and-default))
  ($ (rows xtbl) location))

(defmethod xtbl/row ((xtbl xtbl) columns)
  (unless (= (length columns) (cols-count xtbl))
    (error "Invalid number of columns in row."))
  (setf (rows xtbl) (cons columns (rows xtbl)))
  (setf (cols-widths xtbl)
        (mapcar #'compute-col-width
                columns
                (cols-widths xtbl)
                (cols-formatters xtbl)))
  (values))

(defmethod xtbl/separator ((xtbl xtbl))
  (setf (rows xtbl) (cons nil (rows xtbl)))
  (values))

(defun string-pad (text len)
  (format nil "~v<~a~>" len text))

(defun string-pad-right (text len)
  (format nil "~v@<~a~>" len text))

(defun display-header (xtbl out)
  (let* ((header (header xtbl))
         (header-len (length header))
         (widths (cols-widths xtbl))
         (count (cols-count xtbl))
         (len (1- (+ count (reduce #'+ widths))))
         (content
           (if (>= header-len len) header
               (let ((left-margin (+ header-len (floor (/ (- len header-len) 2)))))
                 (string-pad header left-margin)))))
    (format out ".~a.~%" (make-string len :initial-element #\-))
    (format out "|~a|~%" (string-pad-right content len))))

(defun display-separator (xtbl out)
  (loop for len in (cols-widths xtbl)
        do (format out "+~a" (make-string len :initial-element #\-)))
  (format out "+~%"))

(defun display-col (value len out &optional (formatter *default-value-formatter*))
  (let ((formatted-value (funcall formatter value)))
    (if (numberp value)
        (format out "|~a " (string-pad formatted-value (1- len)))
        (if value
            (format out "| ~a" (string-pad-right formatted-value (1- len)))
            (format out "| ~a" (string-pad-right "   " (1- len)))))))

(defun display-row (xtbl row out)
  (if row
      (progn
        (map nil (lambda (value len formatter)
                   (display-col value len out formatter))
             row
             (cols-widths xtbl)
             (cols-formatters xtbl))
        (format out "|~%"))
      (display-separator xtbl out)))

(defmethod xtbl/display ((xtbl xtbl) &optional (out *standard-output*))
  (when (header xtbl)
    (display-header xtbl out))
  (display-separator xtbl out)
  (display-row xtbl (cols xtbl) out)
  (display-separator xtbl out)
  (if (has-tail-p xtbl)
      (loop for row in (reverse (cdr (rows xtbl))) do
        (display-row xtbl row out))
      (loop for row in (reverse (rows xtbl)) do
        (display-row xtbl row out)))
  (display-separator xtbl out)
  (when (has-tail-p xtbl)
    (display-row xtbl (car (rows xtbl)) out)
    (display-separator xtbl out)))

(defmethod print-object ((obj xtbl) out)
  (print-unreadable-object (obj out :type t)
    (format out "HEADER:'~a' COLUMNS:~a" (header obj) (cols obj))))

(defun xtbl/test ()
  (let ((xtbl (xtbl '("Name  " "Age  "))))
    (format t "XTBL : ~a~%~%" xtbl)
    (xtbl/row xtbl '("Bob" 42))
    (xtbl/row xtbl '("Bill" 12))
    (xtbl/separator xtbl)
    (xtbl/row xtbl '("Jane" 23.12234))
    (xtbl/display xtbl)))

(defun plist/labels (row)
  (loop :for i :from 0 :below ($count row)
        :for v :in row
        :when (evenp i)
          :collect v))

(defun plist/values (row)
  (loop :for i :from 0 :below ($count row)
        :for v :in row
        :when (oddp i)
          :collect v))

;; DISPLAY PLIST AS A TABLE
(defun prn/table (plist &key tailp)
  (let ((n ($count (car plist))))
    (when (evenp n)
      (let ((columns (plist/labels (car plist))))
        (let ((tbl (xtbl columns :tailp tailp)))
          (loop :for rec :in plist
                :for vals = (plist/values rec)
                :do (xtbl/row tbl vals))
          (prn "")
          (xtbl/display tbl))))))

(defun prn/a (as &key start end title std)
  (let ((mks #{"01" :JAN "02" :FEB "03" :MAR "04" :APR "05" :MAY "06" :JUN
               "07" :JUL "08" :AUG "09" :SEP "10" :OCT "11" :NOV "12" :DEC})
        (yrs (let ((start-year (parse/integer (subseq start 0 4)))
                   (end-year (parse/integer (subseq end 0 4))))
               (loop :for y :from start-year :to end-year :collect y))))
    (let ((od (loop :for yr :in yrs
                    :collect (append (list :year yr)
                                     (loop :for mk :in (hash-table-keys mks)
                                           :for date = (format nil "~A-~A" yr mk)
                                           :for rec = (->> as
                                                           (tx/filter `(:eq :date ,date))
                                                           (car))
                                           :append (list ($ mks mk)
                                                         (if rec
                                                             (if std
                                                                 ($ rec :dm)
                                                                 ($ rec :mf))
                                                             nil)))))))
      (when title (prn (format nil "~%[~A]" title)))
      (if (>= ($count as) 12)
          (prn/table (->> od
                          (mappend/marginals (list :year :all)))
                     :tailp T)
          (prn/table (->> od
                          (mappend/marginals.row (list :year :all))))))
    as))
