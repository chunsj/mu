(in-package :mu)

(defun newlabel (column header)
  (cond ((listp column) (cadr column))
        (T (strkw (strcat header (string-upcase (symbol-name column)))))))

(defun mappend/tx.log (column plists)
  (let ((nl (newlabel column "LOG-"))
        (col (if (listp column) (car column) column)))
    (loop :for plist :in plists
          :for v = ($ plist col)
          :when v
            :collect (append plist (list nl (log v))))))

(defun mappend/tx.log1p (column plists)
  (let ((nl (newlabel column "LOG1P-"))
        (col (if (listp column) (car column) column)))
    (loop :for plist :in plists
          :for v = ($ plist col)
          :when v
            :collect (append plist (list nl (if (zerop (1+ v))
                                                (log 1E-6)
                                                (log (1+ v))))))))

(defun mappend/tx.zs (column plists)
  (let ((nl (newlabel column "ZS-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let ((mv (mean vs))
            (sv (sd vs)))
        (if (zerop sv)
            (loop :for plist :in plists
                  :for v = ($ plist col)
                  :for z = 0.0
                  :when v
                    :collect (append plist (list nl z)))
            (loop :for plist :in plists
                  :for v = ($ plist col)
                  :for z = (* 1.0 (/ (- v mv) sv))
                  :when v
                    :collect (append plist (list nl z))))))))

(defun mappend/tx.ms (column plists)
  (let ((nl (newlabel column "MS-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let* ((mv (median vs))
             (sv (/ (->> (mapcar (lambda (v) (* (- v mv) (- v mv))) vs)
                         (reduce #'+))
                    (1- ($count vs)))))
        (loop :for plist :in plists
              :for v = ($ plist col)
              :when v
                :collect (append plist (list nl (* 1.0 (/ (- v mv) sv)))))))))

(defun mappend/tx.unit (column plists)
  (let ((nl (newlabel column "U1-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let ((minv (min* vs))
            (maxv (max* vs)))
        (loop :for plist :in plists
              :for v = ($ plist col)
              :when v
                :collect (append plist (list nl (* 1.0 (/ (- v minv) (- maxv minv))))))))))

(defun mappend/tx.uniz (column plists)
  (let ((nl (newlabel column "UZ-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let ((minv (min* vs))
            (maxv (max* vs)))
        (loop :for plist :in plists
              :for v = ($ plist col)
              :when v
                :collect (append plist (list nl (- (/ (- v minv) (- maxv minv)) 0.5))))))))

(defun mappend/tx.median (column plists)
  (let ((nl (newlabel column "MD-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let ((mv (median vs)))
        (loop :for plist :in plists
              :for v = ($ plist col)
              :when v
                :collect (append plist (list nl (- v mv))))))))

(defun mappend/tx.mean (column plists)
  (let ((nl (newlabel column "MN-"))
        (col (if (listp column) (car column) column)))
    (let ((vs (tx/map col plists)))
      (let ((mv (mean vs)))
        (loop :for plist :in plists
              :for v = ($ plist col)
              :when v
                :collect (append plist (list nl (- v mv))))))))

(defun tx/scale (spec plists)
  (let ((way (if (listp spec)
                 (if (>= ($count spec) 2)
                     (cadr spec)
                     :zs)))
        (args (if (listp spec)
                  (cond ((= 1 ($count spec)) (car spec))
                        ((= 2 ($count spec)) (car spec))
                        ((= 3 ($count spec)) (list (car spec) (caddr spec)))
                        (T nil)))))
    (if args
        (cond ((eq way :zs) (mappend/tx.zs args plists))
              ((eq way :ms) (mappend/tx.ms args plists))
              ((eq way :ut) (mappend/tx.unit args plists))
              ((eq way :uz) (mappend/tx.uniz args plists))
              ((eq way :dz) (mappend/tx.mean args plists))
              ((eq way :dm) (mappend/tx.median args plists))
              ((eq way :lg) (mappend/tx.log args plists))
              ((eq way :l1) (mappend/tx.log1p args plists))
              (T plists))
        plists)))

(defun mappend/rank (rank-field plists)
  (tx/mappend (lambda (plist)
                (when-let ((rank ($ plist rank-field)))
                  (list :rank rank)))
              plists))

(defun gen/row-marginals (excepts records)
  (let ((excepts (if (listp excepts) excepts (list excepts))))
    (loop :for record :in records
          :for keys = (loop :for key :in record :by #'cddr :collect key)
          :for values = (loop :for k :in keys
                              :for v = ($ record k)
                              :unless (or (null v) (find k excepts :test #'equal))
                                :collect v)
          :collect (append record (list :mgn (floor (mean values)))))))

(defun gen/col-marginals (excepts records)
  (let ((excepts (list (car (if (listp excepts) excepts (list excepts))))))
    (let ((keys (loop :for key :in (car records) :by #'cddr :collect key)))
      (append records
              (list (append (loop :for k :in excepts
                                  :appending (list k "MGN"))
                            (loop :for k :in keys
                                  :for vs = (loop :for record :in records
                                                  :for v = ($ record k)
                                                  :when v
                                                    :collect v)
                                  :unless (find k excepts :test #'equal)
                                    :appending (list k (floor (mean vs))))))))))

(defun mappend/marginals.row (index records)
  (->> records
       (gen/row-marginals index)))

(defun mappend/marginals (index records)
  (->> records
       (gen/row-marginals index)
       (gen/col-marginals index)))

(defun tx/percent (&rest args)
  (let ((excepts (if (= 2 ($count args)) (car args) nil))
        (records (if (= 2 ($count args)) (cadr args) (car args))))
    (let ((keys (loop :for k :in (car records) :by #'cddr :collect k))
          (excepts (if (listp excepts) excepts (list excepts))))
      (loop :for record :in records
            :collect (loop :for k :in keys
                           :for v = ($ record k)
                           :appending (list k (if (numberp v)
                                                  (if (find k excepts :test #'equal)
                                                      v
                                                      (round (* 100 v)))
                                                  v)))))))
