(in-package :mu)

(defgeneric tx/eq (av bv))
(defgeneric tx/lt (av bv))
(defgeneric tx/gt (av bv))
(defgeneric tx/le (av bv))
(defgeneric tx/ge (av bv))
(defgeneric tx/ne (av bv))

(defmethod tx/eq ((av NULL) (bv NULL)) nil)
(defmethod tx/lt ((av NULL) (bv NULL)) nil)
(defmethod tx/gt ((av NULL) (bv NULL)) nil)
(defmethod tx/le ((av NULL) (bv NULL)) nil)
(defmethod tx/ge ((av NULL) (bv NULL)) nil)
(defmethod tx/ne ((av NULL) (bv NULL)) nil)

(defmethod tx/eq ((av number) (bv number)) (= av bv))
(defmethod tx/lt ((av number) (bv number)) (< av bv))
(defmethod tx/gt ((av number) (bv number)) (> av bv))
(defmethod tx/le ((av number) (bv number)) (<= av bv))
(defmethod tx/ge ((av number) (bv number)) (>= av bv))
(defmethod tx/ne ((av number) (bv number)) (not (= av bv)))
(defmethod tx/eq ((av NULL) (bv number)) nil)
(defmethod tx/lt ((av NULL) (bv number)) nil)
(defmethod tx/gt ((av NULL) (bv number)) T)
(defmethod tx/le ((av NULL) (bv number)) nil)
(defmethod tx/ge ((av NULL) (bv number)) T)
(defmethod tx/ne ((av NULL) (bv number)) T)
(defmethod tx/eq ((av number) (bv NULL)) nil)
(defmethod tx/lt ((av number) (bv NULL)) T)
(defmethod tx/gt ((av number) (bv NULL)) nil)
(defmethod tx/le ((av number) (bv NULL)) T)
(defmethod tx/ge ((av number) (bv NULL)) nil)
(defmethod tx/ne ((av number) (bv NULL)) T)

(defmethod tx/eq ((av string) (bv string)) (string= av bv))
(defmethod tx/lt ((av string) (bv string)) (string< av bv))
(defmethod tx/gt ((av string) (bv string)) (string> av bv))
(defmethod tx/le ((av string) (bv string)) (string<= av bv))
(defmethod tx/ge ((av string) (bv string)) (string>= av bv))
(defmethod tx/ne ((av string) (bv string)) (not (string= av bv)))
(defmethod tx/eq ((av NULL) (bv string)) nil)
(defmethod tx/lt ((av NULL) (bv string)) nil)
(defmethod tx/gt ((av NULL) (bv string)) T)
(defmethod tx/le ((av NULL) (bv string)) nil)
(defmethod tx/ge ((av NULL) (bv string)) T)
(defmethod tx/ne ((av NULL) (bv string)) T)
(defmethod tx/eq ((av string) (bv NULL)) nil)
(defmethod tx/lt ((av string) (bv NULL)) T)
(defmethod tx/gt ((av string) (bv NULL)) nil)
(defmethod tx/le ((av string) (bv NULL)) T)
(defmethod tx/ge ((av string) (bv NULL)) nil)
(defmethod tx/ne ((av string) (bv NULL)) T)

(defmethod tx/eq ((av symbol) (bv symbol)) (string= av bv))
(defmethod tx/lt ((av symbol) (bv symbol)) (string< av bv))
(defmethod tx/gt ((av symbol) (bv symbol)) (string> av bv))
(defmethod tx/le ((av symbol) (bv symbol)) (string<= av bv))
(defmethod tx/ge ((av symbol) (bv symbol)) (string>= av bv))
(defmethod tx/ne ((av symbol) (bv symbol)) (not (string= av bv)))
(defmethod tx/eq ((av NULL) (bv symbol)) nil)
(defmethod tx/lt ((av NULL) (bv symbol)) nil)
(defmethod tx/gt ((av NULL) (bv symbol)) T)
(defmethod tx/le ((av NULL) (bv symbol)) nil)
(defmethod tx/ge ((av NULL) (bv symbol)) T)
(defmethod tx/ne ((av NULL) (bv symbol)) T)
(defmethod tx/eq ((av symbol) (bv NULL)) nil)
(defmethod tx/lt ((av symbol) (bv NULL)) T)
(defmethod tx/gt ((av symbol) (bv NULL)) nil)
(defmethod tx/le ((av symbol) (bv NULL)) T)
(defmethod tx/ge ((av symbol) (bv NULL)) nil)
(defmethod tx/ne ((av symbol) (bv NULL)) T)

(defun txsym (kw)
  (-> (concatenate 'string
                   (symbol-name kw))
      (string-upcase)
      (intern (string-upcase (package-name *package*)))))

(defun txpair (a b) (list (list a b)))
(defun txgetf (k) (list 'getf '__prplst__ k))
(defun txignr (k) (txpair 'declare (list 'ignorable (txsym k))))
(defun txpairs (keys) (loop :for k :in keys :append (txpair (txsym k) (txgetf k))))
(defun txignrs (keys) (loop :for k :in keys :append (txignr k)))

(defmacro with-fields ((&rest fields) &body body)
  `(lambda (__prplst__)
     (when-let (,@(txpairs fields))
       ,@(txignrs fields)
       ,@body)))

(defun tx-map-plist-function (spec)
  (typecase spec
    (function spec)
    (list (lambda (plist)
            (let ((res (mapcar (lambda (k) ($ plist k)) spec)))
              (when (every #'identity res)
                res))))
    (T (lambda (plist) (getf plist spec)))))

(defun tx-filter-elements-by-spec (spec)
  (let ((cmd (car spec))
        (args (cdr spec))
        (key (cadr spec))
        (arg (cadr spec))
        (val (caddr spec)))
    (cond ((eq cmd :eq) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (tx/eq av v))))
          ((eq cmd :ne) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (not (tx/eq av v)))))
          ((eq cmd :lt) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (tx/lt av v))))
          ((eq cmd :gt) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (tx/gt av v))))
          ((eq cmd :le) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (tx/le av v))))
          ((eq cmd :ge) (lambda (a)
                          (when-let ((av (getf a key))
                                     (v val))
                            (tx/ge av v))))
          ((eq cmd :null) (lambda (a)
                            (null (getf a key))))
          ((eq cmd :or) (lambda (a) (some #'identity
                                          (->> (mapcar #'tx-filter-plist-function args)
                                               (mapcar (lambda (f) (funcall f a)))))))
          ((eq cmd :and) (lambda (a) (every #'identity
                                            (->> (mapcar #'tx-filter-plist-function args)
                                                 (mapcar (lambda (f) (funcall f a)))))))
          ((eq cmd :not) (lambda (a) (not (funcall (tx-filter-plist-function arg) a)))))))

(defun tx-filter-plist-function (spec)
  (typecase spec
    (function spec)
    (list (tx-filter-elements-by-spec spec))
    (T (lambda (plist) (getf plist spec)))))

(defun tx-sort-plist-by-key (key &optional (order :asc))
  (cond ((eq order :asc) (lambda (a b) (tx/lt (getf a key) (getf b key))))
        ((eq order :desc) (lambda (a b) (tx/gt (getf a key) (getf b key))))
        (T (lambda (a b) (tx/lt (getf a key) (getf b key))))))

(defun tx-sort-plist-function (spec)
  (typecase spec
    (function spec)
    (list (tx-sort-plist-by-key (car spec) (cadr spec)))
    (T (tx-sort-plist-by-key spec))))

(defun tx-sort-and-rank-plist (spec plists)
  (let ((rank-col (when (and (listp spec) (= 3 ($count spec))) (caddr spec))))
    (when-let ((sorted-plists (sort (copy-list plists) (tx-sort-plist-function spec))))
      (if rank-col
          (loop :for plist :in sorted-plists
                :for rank :from 1
                :collect (append plist (list rank-col rank)))
          sorted-plists))))

(defun tx-uniq-plist-function (spec)
  (typecase spec
    (function spec)
    (list (lambda (s1 s2) (tx/eq (getf s1 (car spec)) (getf s2 (cadr spec)))))
    (T (lambda (s1 s2) (tx/eq (getf s1 spec) (getf s2 spec))))))

(defun tx/distinct (seq) (remove-duplicates seq :test #'equal))

(defun tx/map (function plists)
  (if function
      (loop :for plist :in plists
            :for nplist = (funcall (tx-map-plist-function function) plist)
            :when nplist
              :collect nplist)
      (copy-list plists)))

(defun tx/map.index (function plists)
  (declare (special *index*))
  (setf *index* -1)
  (tx/map (lambda (plist)
            (incf *index*)
            (funcall (tx-map-plist-function function) plist))
          plists))

(defun tx/select (fields plists)
  (loop :for plist :in plists
        :for vs = (mapcar (lambda (f) ($ plist f)) fields)
        :collect (loop :for f :in fields
                       :for v :in vs
                       :appending (list f v))))

(defun tx/mappend (function plists)
  (if function
      (loop :for plist :in plists
            :for record = (funcall (tx-map-plist-function function) plist)
            :when record
              :collect (append plist record))
      (copy-list plists)))

(defun tx/replace (function plists)
  (if function
      (loop :for plist :in plists
            :for record = (funcall (tx-map-plist-function function) plist)
            :when record
              :collect (let ((newrec (copy-list plist)))
                         (loop :for k :in record :by #'cddr
                               :for v = ($ record k)
                               :do (if ($ newrec k)
                                       (setf (getf newrec k) v)
                                       (setf newrec (append newrec (list k v)))))
                         newrec))
      (copy-list plists)))

(defun tx/filter (function plists)
  (if function
      (remove-if-not (tx-filter-plist-function function) plists)
      (copy-list plists)))

(defun tx/uniq (function plists)
  (if function
      (remove-duplicates plists :test (tx-uniq-plist-function function))
      (copy-list plists)))

(defun tx/sort (function plists)
  (if function
      (tx-sort-and-rank-plist function plists)
      (copy-list plists)))

(defun apply/agf (agf agx entries)
  (if agx
      (cond ((eq agf :mean) (mean (tx/map agx entries)))
            ((eq agf :median) (median (tx/map agx entries)))
            ((eq agf :sd) (sd (tx/map agx entries)))
            ((eq agf :gmean1p) (gmean1p (tx/map agx entries)))
            ((eq agf :sum) (sum (tx/map agx entries)))
            ((eq agf :count) ($count (tx/map agx entries))))
      (funcall agf entries)))

(defun tx/group (function plists)
  (cond ((eq function :year)
         (let ((years (->> plists
                           (tx/map (lambda (plist) (subseq ($ plist :date) 0 4)))
                           (tx/distinct))))
           (loop :for year :in (tx/sort #'string< years)
                 :for entries = (tx/filter (lambda (plist)
                                             (string= year (subseq ($ plist :date) 0 4)))
                                           plists)
                 :collect (list :year year :entries entries))))
        ((eq function :month)
         (let ((months (->> plists
                            (tx/map (lambda (plist) (subseq ($ plist :date) 5 7)))
                            (tx/distinct))))
           (loop :for month :in (tx/sort #'string< months)
                 :for entries = (tx/filter (lambda (plist)
                                             (string= month (subseq ($ plist :date) 5 7)))
                                           plists)
                 :collect (list :month month :entries entries))))
        ((keywordp function)
         (let ((fvalues (->> plists
                             (tx/map function)
                             (tx/distinct))))
           (when fvalues
             (loop :for fvalue :in (tx/sort #'string< fvalues)
                   :for entries = (tx/filter (lambda (plist)
                                               (equal fvalue ($ plist function)))
                                             plists)
                   :collect (list function fvalue :entries entries)))))
        ((listp function)
         (let ((ax1 (car function))
               (ax2 (cadr function))
               (agf (or (caddr function) #'identity))
               (agx (cadddr function)))
           (->> (tx/group ax1 plists)
                (tx/map (lambda (e)
                          (let ((av1 ($ e ax1))
                                (entries ($ e :entries)))
                            (apply #'append (list ax1 av1)
                                   (->> (tx/group ax2 entries)
                                        (tx/map (lambda (ee)
                                                  (let ((v2 ($ ee ax2))
                                                        (ees ($ ee :entries)))
                                                    (list v2 (apply/agf agf agx ees)))))))))))))
        (T (let ((fvalues (->> plists
                               (tx/map function)
                               (tx/distinct))))
             (when fvalues
               (loop :for fvalue :in (tx/sort #'string< fvalues)
                     :for entries = (tx/filter (lambda (plist)
                                                 (equal fvalue (funcall function plist)))
                                               plists)
                     :collect (list :group fvalue :entries entries)))))))

(defun tx/take (n plists)
  (cond ((>= n 1) (loop :for plist :in plists
                        :for i :from 1 :to n
                        :collect plist))
        ((and (> n 0) (< n 1)) (let ((m (round (* n ($count plists)))))
                                 (loop :for plist :in plists
                                       :for i :from 1 :to m
                                       :collect plist)))
        ((<= n -1) (->> plists
                        (reverse)
                        (tx/take (abs n))
                        (reverse)))
        (T nil)))

(defun tx/drop (n plists)
  (let ((count ($count plists)))
    (cond ((>= n 1) (loop :for plist :in plists
                          :for i :from 1 :to count
                          :when (> i n)
                            :collect plist))
          ((and (> n 0) (< n 1)) (let ((m (round (* n ($count plists)))))
                                   (loop :for plist :in plists
                                         :for i :from 1 :to count
                                         :when (> i m)
                                           :collect plist)))
          ((zerop n) plists)
          (T nil))))

(defun tx/quantiles (&rest args)
  (if (and (= 1 ($count args)) (listp (car args)))
      (let* ((data (car args))
             (n ($count data))
             (nq (floor (/ n 5))))
        (loop :for q :from 0 :below 5
              :collect (subseq data (* q nq) (+ (* q nq) nq))))
      (let* ((qsz (car args))
             (data (cadr args))
             (n ($count data))
             (nq (floor (/ n qsz))))
        (loop :for q :from 0 :below qsz
              :collect (subseq data (* q nq) (+ (* q nq) nq))))))

(defun tx/mean (field plists) (mean (tx/map field plists)))
(defun tx/sd (field plists) (sd (tx/map field plists)))

(defun tx/pmap (function plists)
  (if function
      (->> (lparallel:pmapcar (lambda (plist)
                                (funcall (mu::tx-map-plist-function function) plist))
                              plists)
           (tx/filter (lambda (e) e)))
      (copy-list plists)))
